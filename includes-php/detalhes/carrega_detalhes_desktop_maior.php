<?php    	
	if (isset($_GET['busca']))
		$complemento_link = $_GET['busca'];
	else
		echo header("location: index.php");
	
	//Abre banco de dados
	require_once("includes-php/compartilhado/conexao.php");    
	
	//Informações gerais da empresa
	$query = "SELECT idEmpresa,nomeEmpresa,empresas.descricaoEmpresa,empresas.linkSiteEmpresa,empresas.linkLogoEmpresa,empresas.fazEntregaEmpresa 
	FROM empresas 
	WHERE statusEmpresa = '1' and complementoLinkEmVicosa = '$complemento_link'";
	$result1 = mysql_query($query,$connection);
	
	$id_empresa = mysql_result($result1,0,0);
	
	//Número de indicações positivas
	$query = "SELECT numTotalIndicacoesPositivas 
	FROM viewnumtotalindicacoesdasempresasativas 
	WHERE idEmpresa = '$id_empresa'";
	$result2 = mysql_query($query,$connection) or die (mysql_error());	
	
	//Fotos da empresa
	$query = "SELECT linkFotoDaEmpresa 
	FROM fotosdasempresas 
	WHERE statusFotoDaEmpresa = '1' and idEmpresa = '$id_empresa'";
	$result3 = mysql_query($query,$connection);		
	
	//Horários de funcionamento
	$query = "SELECT diaSemana,horaAbertura,horaFechamento 
	FROM viewhorariosdefuncionamentodasempresasativas 
	WHERE idEmpresa = '$id_empresa'";
	$result4 = mysql_query($query,$connection);			
	
	//Endereços da empresa
	$query = "SELECT logradouroEndereco,numeroEndereco,complementoEndereco,bairroEndereco,latitudeEndereco,longitudeEndereco 
	FROM viewenderecosdasempresasativas 
	WHERE idEmpresa = '$id_empresa'";
	$result5 = mysql_query($query,$connection);
	
	//Formas de pagamento
	$query = "SELECT descricaoFormaDePagamentoAceita,classeCssFormaDePagamentoAceita 
	FROM viewformasdepagamentosaceitaspelasempresasativas 
	WHERE idEmpresa = '$id_empresa'";
	$result6 = mysql_query($query,$connection);

	//Media das avaliações
	$query = "SELECT totalSomaDasAvaliacoes,numAvaliacoes,mediaAvaliacao 
	FROM viewmediaavaliacaodasempresasativas 
	WHERE idEmpresaAvaliacao = '$id_empresa'";
	$result7 = mysql_query($query,$connection);
	
	//Planos de saúde
	$query = "SELECT nomePlanoDeSaude,classeCssPlanoDeSaude 
	FROM viewplanosdesaudedasempresasativas 
	WHERE idEmpresa = '$id_empresa'";
	$result8 = mysql_query($query,$connection);

	//Redes sociais
	$query = "SELECT nomeRedeSocial,linkRedeSocialDaEmpresa,classeCssRedeSocial 
	FROM viewredessociaisdasempresasativas 
	WHERE idEmpresa = '$id_empresa'";
	$result9 = mysql_query($query,$connection);

	//Telefones
	$query = "SELECT dddTelefone,numeroTelefone,ehWhatsapp,descricaoTipoTelefone,nomeOperadora,classeCssOperadora 
	FROM viewtelefonesdasempresasativas 
	WHERE idEmpresa = '$id_empresa'";
	$result10 = mysql_query($query,$connection);
	
	//Informações gerais da empresa
	$nome_empresa = mysql_result($result1,0,1);
	$descricao_empresa = mysql_result($result1,0,2);
	$link_site_empresa = mysql_result($result1,0,3);
	$link_logo_empresa = mysql_result($result1,0,4);
	$faz_entrega_empresa = mysql_result($result1,0,5);
	
	//Número de indicações da empresa
	if (mysql_num_rows($result2) == 1)
		$num_total_indicacoes_positivas_empresa = mysql_result($result2,0,0);
	else
		$num_total_indicacoes_positivas_empresa = 0;
	
	//Fotos da empresa
	$num_fotos_empresa = mysql_num_rows($result3);
	$link_foto_empresa = array();
	for ($i = 0; $i < $num_fotos_empresa; $i++)
		$link_foto_empresa[$i] = mysql_result($result3,$i,0);
	
	//Horários de funcionamento
	//diaSemana   horaAbertura   horaFechamento 
	$num_horarios_funcionamento_empresa = mysql_num_rows($result4);
	$horario_funcionamento_empresa = array(array());
	for ($i = 0; $i < $num_horarios_funcionamento_empresa; $i++){
		$horario_funcionamento_empresa[$i][0] = mysql_result($result4,$i,0);
		$horario_funcionamento_empresa[$i][1] = mysql_result($result4,$i,1);
		$horario_funcionamento_empresa[$i][2] = mysql_result($result4,$i,2);
	}
	
	//Endereços das empresas
	//logradouroEndereco  numeroEndereco  complementoEndereco  bairroEndereco  latitudeEndereco  longitudeEndereco
	$num_enderecos_empresa = mysql_num_rows($result5);
	$enderecos_empresa = array(array());
	for ($i = 0; $i < $num_enderecos_empresa; $i++){
		$enderecos_empresa[$i][0] = mysql_result($result5,$i,0);
		$enderecos_empresa[$i][1] = mysql_result($result5,$i,1);
		$enderecos_empresa[$i][2] = mysql_result($result5,$i,2);
		$enderecos_empresa[$i][3] = mysql_result($result5,$i,3);
		$enderecos_empresa[$i][4] = mysql_result($result5,$i,4);
		$enderecos_empresa[$i][5] = mysql_result($result5,$i,5);
	}	

	//Formas de pagamento
	//descricaoFormaDePagamentoAceita  classeCssFormaDePagamentoAceita 
	$num_formas_pagamento_empresa = mysql_num_rows($result6);
	$formas_pagamento_empresa = array(array());
	for ($i = 0; $i < $num_formas_pagamento_empresa; $i++){
		$formas_pagamento_empresa[$i][0] = mysql_result($result6,$i,0);
		$formas_pagamento_empresa[$i][1] = mysql_result($result6,$i,1);
	}
	
	//Média das avaliações da empresa
	if (mysql_num_rows($result7) == 1)
		$media_avaliacoes_empresa = mysql_result($result7,0,0);
	else
		$media_avaliacoes_empresa = 0;
	
	//Planos de saúde da empresa
	//nomePlanoDeSaude  classeCssPlanoDeSaude 
	$num_planos_saude_empresa = mysql_num_rows($result8);
	$planos_saude_empresa = array(array());
	for ($i = 0; $i < $num_planos_saude_empresa; $i++){
		$planos_saude_empresa[$i][0] = mysql_result($result8,$i,0);
		$planos_saude_empresa[$i][1] = mysql_result($result8,$i,1);
	}
	
	//Redes sociais da empresa
	//nomeRedeSocial  linkRedeSocialDaEmpresa  classeCssRedeSocial 
	$num_redes_sociais_empresa = mysql_num_rows($result9);
	$redes_sociais_empresa = array(array());
	for ($i = 0; $i < $num_redes_sociais_empresa; $i++){
		$redes_sociais_empresa[$i][0] = mysql_result($result9,$i,0);
		$redes_sociais_empresa[$i][1] = mysql_result($result9,$i,1);
		$redes_sociais_empresa[$i][2] = mysql_result($result9,$i,2);
	}
	
	//Telefones da empresa
	//dddTelefone  numeroTelefone  ehWhatsapp  descricaoTipoTelefone  nomeOperadora  classeCssOperadora 
	$num_telefones_empresa = mysql_num_rows($result10);
	$telefones_empresa = array(array());
	for ($i = 0; $i < $num_telefones_empresa; $i++){
		$telefones_empresa[$i][0] = mysql_result($result10,$i,0);
		$telefones_empresa[$i][1] = mysql_result($result10,$i,1);
		$telefones_empresa[$i][2] = mysql_result($result10,$i,2);
		$telefones_empresa[$i][3] = mysql_result($result10,$i,3);
		$telefones_empresa[$i][4] = mysql_result($result10,$i,4);
		$telefones_empresa[$i][5] = mysql_result($result10,$i,5);
	}	
	
	//------------------Avaliações------------------------
	$pagina = 1;
		
	$num_de_empresas = 9;
	
	$offset = ($num_de_empresas*($pagina-1));
	
	//Busca avaliações
	$query = "SELECT usuarios.nomeUsuario,usuarios.linkFotoFacebook,avaliacoes.notaAvaliacao,avaliacoes.justificativaAvaliacao,avaliacoes.dataAvaliacao 
	FROM avaliacoes,empresas,usuarios 
	WHERE avaliacoes.idEmpresa = empresas.idEmpresa and avaliacoes.idUsuario = usuarios.idUsuario and empresas.complementoLinkEmVicosa = '$complemento_link' 
	ORDER BY avaliacoes.dataAvaliacao LIMIT $num_de_empresas OFFSET $offset";
	$result_comentarios1 = mysql_query($query,$connection) or die(mysql_error());

	//Busca total de avaliações
	$query = "SELECT usuarios.nomeUsuario 
	FROM avaliacoes,empresas,usuarios 
	WHERE avaliacoes.idEmpresa = empresas.idEmpresa and avaliacoes.idUsuario = usuarios.idUsuario and empresas.complementoLinkEmVicosa = '$complemento_link' 
	ORDER BY avaliacoes.dataAvaliacao";
	$result_comentarios2 = mysql_query($query,$connection) or die(mysql_error());
	
	$resposta = array();
	
	//Guarda número total de avaliações
	$num_total_avaliacoes = mysql_num_rows($result_comentarios2);
	
	$row = array();
	$i = 0;
	while ($row = mysql_fetch_assoc($result_comentarios1)) {
		$resposta[$i+1] = $row["nomeUsuario"];
		$resposta[$i+2] = $row["linkFotoFacebook"];
		$resposta[$i+3] = $row["notaAvaliacao"];
		$resposta[$i+4] = $row["justificativaAvaliacao"];
		$resposta[$i+5] = $row["dataAvaliacao"];			
		$i+=5;
	}
	//----------------------------------------------------
	
	$aux = "<div class='item-detalhes-slider-botoes'>
	<div class='item-slider-indicacao'>
	<div class='item-slider'>
	<ul class='bxslider'>";
	
	if ($num_fotos_empresa == 0)
		$aux .= "<li><img id = '".$i."' class='imagem-detalhes' src='img/mg/vicosa/estabelecimentos/".$link_logo_empresa."' /></li>";
	for ($i = 0; $i < $num_fotos_empresa; $i++){
		$aux .= "<li><img id = '".$i."' class='imagem-detalhes' src='img/mg/vicosa/estabelecimentos/".$link_foto_empresa[$i]."'/></li>";
	}
	
	$aux .= "</ul>
             </div>
             <div id='indicacao-d-maior'>
			 <div class='item-area-indicacao-texto'>
             <div class='item-icone-indicacao'>
			 <div class='icone-indicacao'></div>
			 </div>
			 <div class='item-num-indicacao parent'><div class='child'>".($num_total_indicacoes_positivas_empresa+1)." pessoas indicaram este serviço!</div></div>
             </div>
             </div>
             <div id='indicacao-icone-d-maior' class='item-area-indicacao'>
             <div  id = 'item-icone-indicacao' class='item-icone-indicacao'>
             <div class='icone-indicacao'></div>
             </div>
             <button id = 'botao-indicacao-d-maior' onclick = \"indico_d_maior('".$complemento_link."');\" class='botao-indicacao font-botao-indicacao font-botao-detalhes'>Indico</button>
             </div>
             </div>
             <div class='item-botao-popup'>
             <div class='font-detalhes-inicio'>".$nome_empresa."</div>
             <div class='item-horizontal-popup'>
             <div id='item-hFuncionamento-dMaior' class='botao-popup-esquerda parent'>
             <span class='child item-icone-popup'><div class='icone-horario-funcionamento'></div></span>
             <span class='font-botao-popup font-botao-detalhes child'>Horário de funcionamento</span>
             </div>
             <div id='item-mensagem-dMaior' class='botao-popup-direita parent'>
             <span class='child item-icone-popup'><div class='icone-mensagem'></div></span>
             <span class='font-botao-popup font-botao-detalhes child'>Envie-nos uma mensagem</span>
             </div>
             </div>
             <div class='item-horizontal-popup'>
             <div id='item-fPagamento-dMaior' class='botao-popup-esquerda parent'>
             <span class='child item-icone-popup'><div class='icone-forma-de-pagamento'></div></span>
             <span class='font-botao-popup font-botao-detalhes child'>Formas de pagamento</span>
             </div>
             <div id = 'item-sobre-dMaior' class='botao-popup-direita parent'>
             <span class='child item-icone-popup'><div class='icone-sobre'></div></span>
             <span class='font-botao-popup font-botao-detalhes child'>Sobre</span>
             </div>
             </div>";
			 
			 if ($num_planos_saude_empresa > 0){
				$aux .= "
				<div class='item-horizontal-popup'>
				<div id='item-planos-dMaior' class='botao-popup-esquerda parent'>
				<span class='child item-icone-popup'><div class='icone-planos'></div></span>
				<span class='font-botao-popup font-botao-detalhes child'>Planos aceitos</span>
				</div>
				</div>";
			 }
			 $aux .= "
             </div>
             </div>
			 <div class='modal-slider'>
             <ul class='bxslider'>";
	
	//Caso a empresa não possua fotos é usada a sua logo
	if ($num_fotos_empresa == 0)
		$aux .= "<li><img id = '".$i."' class='imagem-detalhes' src='img/mg/vicosa/estabelecimentos/".$link_logo_empresa."' /></li>";
	for ($i = 0; $i < $num_fotos_empresa; $i++){
		$aux .= "<li><img id = '".$i."' class='imagem-detalhes' src='img/mg/vicosa/estabelecimentos/".$link_foto_empresa[$i]."' /></li>";
	}
	
	//Avenida P.H. Rolfs, 500 - Centro, Viçosa - MG, 36570-000
	$aux .= "</ul>
			</div>
			<div class='fundo-informacoes'>
            <div class='item-endereco-icone-botao'>
            <div class='icone-localizacao endereco-localizacao'></div>
            <div class='item-endereco-local-detalhes'>";
	
	for ($i = 0; $i < $num_enderecos_empresa; $i++){
			$endereco = $enderecos_empresa[$i][0].', '.$enderecos_empresa[$i][1].' - '.$enderecos_empresa[$i][3].', Viçosa - MG, 36570-000';
            $aux .= "<div class='font-endereco-localizacao-detalhes parent'>
            <div class='child'>".$enderecos_empresa[$i][0].', '.$enderecos_empresa[$i][1].' - '.$enderecos_empresa[$i][3]."</div>
			<a class='child' href='https://www.google.com.br/maps/place/".$endereco."' target='_blank'>
			<div class='item-botao-mapa'>
			<button class='botao-mapa'>Ver mapa</button>
			</div>
			</a>
			</div>";
	}
	$aux .= "</div>
			</div>";
			
	//Caso a empresa possua telefones
	if ($num_telefones_empresa > 0){
		$telefone = '('.$telefones_empresa[0][0].')'.$telefones_empresa[0][1];
		$aux .= "<div class='item-telefone-font-botao'>
			<div class='item-telefone'>
			<div class='item-icone-telefone parent'><div class='icone-telefone child'></div></div>
			<div class='item-num-telefone parent'><div class='child'>".$telefone."</div></div>";
			
		//Caso a empresa tenha mais de um telefone
		if ($num_telefones_empresa > 1)
			$aux .=	"</div><button class='item-botao-telefone font-botao-detalhes child'>+Telefones</button></div>";
		else
			$aux .=	"</div></div>";
		
		//Caso o telefone seja whatsapp
		for ($i = 0; $i < $num_telefones_empresa; $i++)
			if ($telefones_empresa[$i][2] == 1){
			$telefone = '('.$telefones_empresa[$i][0].')'.$telefones_empresa[$i][1];
			$aux .= "
            <div class='item-whatsapp'>
			<div class='icone-whatsapp'></div>
			<div class='item-num-whatsapp parent'><div class='child'>".$telefone."</div></div>
            </div>";
			break;
		}
	}
	
	//Caso a empresa possua site
	if ($link_site_empresa != "" && $link_site_empresa != 0){
		$aux .= " <div class='item-site'>
			<div class='icone-site'></div>
            <div class='item-num-site parent'>
			<div class='child'>".$link_site_empresa."</div>
            </div>
            </div>";
	}
			
	if ($faz_entrega_empresa == 1){
		$aux .="
            <div class='item-entrega parent'>
			<div class='icone-entrega'></div>
            <div class='item-num-entrega parent'><div class='child'>Entrega em domicílio</div></div>
            </div>";
	}
		$aux .= "<div class='detalhes-icones-socias'>";
			for ($i = 0; $i < $num_redes_sociais_empresa; $i++){
                $aux .= "<div class='detalhes-item-icones-sociais'>
                    <a href='".$redes_sociais_empresa[$i][1]."' target='_blank'>
                        <div class='".$redes_sociais_empresa[$i][2]."'></div>
                    </a>
                </div>";
			}		
		$aux .= "</div>
			<div id='modal-telefone' title='Telefones'>
                <input type='hidden' autofocus/>";
	
	for ($i = 1; $i < $num_telefones_empresa; $i++){
		$telefone = '('.$telefones_empresa[$i][0].')'.$telefones_empresa[$i][1];
		$aux .= "<div class='item-modal-telefones'>
                    <span class='child item-icone-popup'><div class='".$telefones_empresa[$i][5]."'></div></span><span class='font-modal child'>".$telefone."</span>
                </div>";
	}
	
	$aux .= "<div class='item-modal-text'>
                    <span class='font-modal child'>Ao ligar, informe que encontrou o telefone no emViçosa!</span>
                </div>
            </div>
        </div>
		
        <div class='item-fundo-avaliacao-facebook'>
			
            <div id = 'a' class='item-fundo-avaliacao-detalhes'>
			
                    <div class='item-avaliacao-detalhes'>
                        <div id = 'fundo-avaliacao-facebook-d-maior' class='fundo-avaliacao-detalhes'>
						
						<div id = 'avaliacao-facebook-d-maior'>
						
                            <div class='font-titulo-avaliacao-detalhes'>
                                Deixe a sua nota para este serviço!
                            </div>
                            <div class='item-detalhes-estrela-avaliacao parent'>
                                <div id='estrela-A-maior' class='item-icone-estrela-detalhes'>
                                    <div id='eA-maior' class='icone-avaliacao-detalhes'></div>
                                </div>
                                <div id='estrela-B-maior' class='item-icone-estrela-detalhes'>
                                    <div id='eB-maior' class='icone-avaliacao-detalhes'></div>
                                </div>
                                <div id='estrela-C-maior' class='item-icone-estrela-detalhes'>
                                    <div id='eC-maior' class='icone-avaliacao-detalhes'></div>
                                </div>
                                <div id='estrela-D-maior' class='item-icone-estrela-detalhes'>
                                    <div id='eD-maior' class='icone-avaliacao-detalhes'></div>
                                </div>
                                <div id='estrela-E-maior' class='item-icone-estrela-detalhes'>
                                    <div id='eE-maior' class='icone-avaliacao-detalhes'></div>
                                </div>
                            </div>
                            <p id='mensagem-detalhes'><textarea id='fundo-mensagem-detalhes-d-maior' placeholder='Conte-nos como foi sua experiência com este serviço!' required='required' name='mensagem-detalhes'></textarea></p>
                            <div class='item-botao-avaliar-detalhes'>
							<button id = 'botao-avaliar-detalhes-d-maior' type='submit' onclick = \"avaliar_detalhes_d_maior('".$complemento_link."'); return false;\" class='botao-avaliar-detalhes font-botao-detalhes-avaliacao font-botao-detalhes'>Avaliar</button>
							</div>
							
							</div> 
							
                        </div>

						</div>						
			</div>
			
            <div class='fb-page' data-href='https://www.facebook.com/facebooka' data-width='420' data-height='400' data-small-header='false' data-adapt-container-width='true' data-hide-cover='false' data-show-facepile='true' data-show-posts='true'><div class='fb-xfbml-parse-ignore'><blockquote cite='https://www.facebook.com/facebook'><a href='https://www.facebook.com/facebook'>Facebook</a></blockquote></div></div>
        </div>";
		
		$aux .= "
        <div class='item-comentario-avaliacao'>"; 
		
		$num_avaliacoes = (count($resposta)-1)/5;
		
		if ($num_total_avaliacoes > 0)
		$aux .= "
            <div id = 'num-total-avaliacoes-d-maior' class='num-detalhes-avaliacao'>".$num_total_avaliacoes." Avaliações</div>";
			
		$aux .= "
            <div class='item-comentario-avalicoes'>";
		
		$j = 1;
		$cont = 1;
		for ($i = 0; $i < $num_avaliacoes; $i++){
		if ($cont == 3){
			$indice = 2;
			$cont = 0;
		}
		else	
			$indice = 1;
			
		$cont++;
		$aux .= "
                <div class='item-comentario".$indice."'>
                <div id = 'comentario' class='item-modal-num'>
                        <span class='child item-imagem-avaliacao-usuario'>
                            <div class='imagem-usuario'><img src = \"".$resposta[$j+1]."\"/></div>
                            <div class='font-modal-avaliacao-nome'>".$resposta[$j]."</div>
                        </span>
                        <span class='item-avaliacao-comentario-detalhes child'>
                            <div class='font-modal-avaliacao-comentario'>".$resposta[$j+3]."</div>

                            <div class='item-avaliacao-estrelas-detalhes'>
                                <div class='balao-rodape-avaliacao'>

                                    <div class='numero-avaliacao'>
                                        <div class='text-numero-avaliacao'>".$resposta[$j+2]."</div>
                                    </div>

                                    <div class='rodape-estrela'>";
									
				$media = $resposta[$j+2];
				$contador_de_estrelas = 0;
				while ($media > 0){
					$media--;
					if ($media >= 0){
						$aux .= "<div class=\"item-estrela estrela-cheia\"></div>";		
						$contador_de_estrelas++;
					}
					else
						if (($media+0.5) == 0){
							$aux .= "<div class=\"item-estrela estrela-metade\"></div>";	
							$contador_de_estrelas++;
							break;
						}
						else
							if (($media+0.5) > 0){
								$aux .= "<div class=\"item-estrela estrela-cheia\"></div>";	
								$contador_de_estrelas++;
								break;
							}
				}
				for ($k = 0; $k < (5-$contador_de_estrelas); $k++)
					$aux .= "<div class=\"item-estrela estrela-vazia\"></div>";	

					
				list($data,$hora) = explode(" ",$resposta[$j+4]);
				list($ano,$mes,$dia) = explode("-",$data);
				$data_avaliacao = $dia."/".$mes."/".$ano;
				
                        $aux .= "   </div>
                                </div>
                            </div>
                            <div class='item-avaliacao-detalhes-estrela-data'>
                                <div class='font-data-comentario-detalhes child'>".$data_avaliacao."</div>
                            </div>
                        </span>
                </div>
                <div class='borda-separa-comentario'></div>
				</div>";
				$j+=5;
		}
				
		$aux .= "
            </div>
            </div>
        </div> 
		<div id = 'mais-avaliacoes-d-maior'></div>";
		if ($num_total_avaliacoes > 9){
			$aux .= "
			<div id = 'item-botao-mais-avaliados' class='item-botao-mais-avaliados'>
				<button onclick = \"ver_mais_comentarios(".($pagina+1).",'".$complemento_link."');\" id = 'botao-ver-mais-comentarios\' type='button' class='fonte-botao-mais-avaliados botao-mais-avaliados font-botao-detalhes' title='Veja mais comentários!'>Ver Mais</button>
			</div>";
		}
	$aux .= "
    </div>";
	
	echo $aux;
?>