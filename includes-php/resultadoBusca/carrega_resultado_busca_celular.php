<?php
	//Abre banco de dados
	require_once("includes-php/compartilhado/conexao.php");
	
	if (!isset($_GET['busca']))
		echo "<script language= \"JavaScript\">location.href=\"inicial.php\"</script>";
	
	$busca = "%";
	$busca .= $_GET['busca'];
	$busca .= "%";
	
	$string = $_GET['busca'];

	//Remove os conectivos iniciais
	for ($i = 0; $i < 5; $i++){
		$pattern = '/^( )*(a|e|o|da|de|do|para|os|as|um|uns|uma|umas|ao|aos|à|às|dos|das|dum|duns|duma|dumas|em|no|nos|na|nas|num|nuns|numa|numas|por|per|pelo|pelos|pela|pelas|ante|após|até|com|conforme|contra|consoante|desde|durante|em|exceto|entre|mediante|perante|por|salvo|sem|segundo|sob|sobre|trás)( )+/i';
		$replacement = '';
		$string =  preg_replace($pattern, $replacement, $string);
	}
	
	//Remove os conectivos do meio da palavra
	for ($i = 0; $i < 15; $i++){
		$pattern = '/( )+(a|e|o|da|de|do|para|os|as|um|uns|uma|umas|ao|aos|à|às|dos|das|dum|duns|duma|dumas|em|no|nos|na|nas|num|nuns|numa|numas|por|per|pelo|pelos|pela|pelas|ante|após|até|com|conforme|contra|consoante|desde|durante|em|exceto|entre|mediante|perante|por|salvo|sem|segundo|sob|sobre|trás)( )+/i';
		$replacement = ' ';
		$string =  preg_replace($pattern, $replacement, $string);
	}
	
	//Remove os conectivos finais
	for ($i = 0; $i < 5; $i++){
		$pattern = '/( )+(a|e|o|da|de|do|para|os|as|um|uns|uma|umas|ao|aos|à|às|dos|das|dum|duns|duma|dumas|em|no|nos|na|nas|num|nuns|numa|numas|por|per|pelo|pelos|pela|pelas|ante|após|até|com|conforme|contra|consoante|desde|durante|em|exceto|entre|mediante|perante|por|salvo|sem|segundo|sob|sobre|trás)( )*$/i';
		$replacement = '';
		$string =  preg_replace($pattern, $replacement, $string);
	}
	
	//Coloca % nos espaços em branco no início e final da frase
	$pattern = '/[[:space:]]/i';
	$replacement = '%';
	$string =  preg_replace($pattern, $replacement, $string);
	$string .= "%";
	$string = '%'.$string;
	
	//Busca empresas
	$query = "SELECT * 
	FROM (SELECT * FROM viewtopservicos) AS resultado 
	WHERE nomeEmpresa like '$busca' or nomeCategoria like '$busca' or palavrasChavesEmpresa like '$busca' or palavrasChavesEmpresa like '$string' 
	GROUP BY resultado.idCategoria 
	ORDER BY resultado.numTotalIndicacoesPositivas DESC LIMIT 200";
	$result1 = mysql_query($query,$connection) or die(mysql_error());
	
	$num_empresas = mysql_num_rows($result1);
	$endereco = array();
	
	$aux = "";
	$cont2 = 0;
	
	if ($num_empresas == 0){
		 $aux = "<div class=\"parent item-menssagem-erro\">
			<div class=\"child menssagem-erro \"><div class=\"text-menssagem-erro \">Não há resultados para a busca: ".$_GET['busca']."</div></div>
		</div>";
	}
	
	for ($i = 1; $i <= 20; $i++){
		if ($cont2 >= $num_empresas)
			break;
		$cont1 = 0;
		$aux .= "<div id=\"container-resultados-celular".$i."\">";
		while ($cont1 < 5 && $cont2 < $num_empresas){
			$id_empresa = mysql_result($result1,$cont2,0);
			$link_logo = mysql_result($result1,$cont2,2);
			$nome_empresa = mysql_result($result1,$cont2,1);
			$avaliacao = mysql_result($result1,$cont2,12);
			
			if (mysql_result($result1,$cont2,4) == "")
				$indicacoes = 0;
			else
				$indicacoes = mysql_result($result1,$cont2,4);
			
			$complemento_link = mysql_result($result1,$cont2,3);
			
			$avaliacao = number_format($avaliacao,1);
			
			$query = "SELECT resultado2.logradouroEndereco,resultado2.numeroEndereco,resultado2.complementoEndereco,resultado2.bairroEndereco 
			FROM (SELECT * FROM viewenderecosdasempresasativas) AS resultado2 
			WHERE resultado2.idEmpresa = '$id_empresa'";
			$result2 = mysql_query($query,$connection) or die(mysql_error());		
			
			for ($j = 0; $j < mysql_num_rows($result2); $j++)
				if (mysql_result($result2,$j,2) == "")
					$endereco[$j] = mysql_result($result2,$j,0).", ".mysql_result($result2,$j,1).", ".mysql_result($result2,$j,3).", Viçosa - MG";
				else
					$endereco[$j] = mysql_result($result2,$j,0).", ".mysql_result($result2,$j,1).", ".mysql_result($result2,$j,2).", ".mysql_result($result2,$j,3).", Viçosa - MG";
			
        $aux .= "<a href = \"detalhes.php?busca=".$complemento_link."\"> <div class=\"resultado-celular".$i."\">
            <div class=\"mais-avaliados\">
                <div class=\"resultado-balao-mais-avaliados\" title=\"".$nome_empresa."\">
                    <div  class=\"conteudo-balao\">
                        <div class=\"item-conteudo\"><img src=\"img/mg/vicosa/estabelecimentos/".$link_logo."\"></div>
                    </div>
                    <div class=\"rodape-balao\">
                        <div class=\"rodape-avaliacao\">
                            <div class=\"balao-rodape-avaliacao\">

                                <div class=\"numero-avaliacao\">
                                    <div class=\"text-numero-avaliacao\">".$avaliacao."</div>
                                </div>

                                <div class=\"rodape-estrela\">";
				$contador_de_estrelas = 0;
				$media = $avaliacao;
				while ($media > 0){
					$media--;
					if ($media >= 0){
						$aux .= "<div class=\"item-estrela estrela-cheia\"></div>";		
						$contador_de_estrelas++;
					}
					else
						if (($media+0.5) == 0){
							$aux .= "<div class=\"item-estrela estrela-metade\"></div>";	
							$contador_de_estrelas++;
							break;
						}
						else
							if (($media+0.5) > 0){
								$aux .= "<div class=\"item-estrela estrela-cheia\"></div>";	
								$contador_de_estrelas++;
								break;
							}
				}
				for ($k = 0; $k < (5-$contador_de_estrelas); $k++)
					$aux .= "<div class=\"item-estrela estrela-vazia\"></div>";
									
                                $aux .= "</div>
                            </div>
                        </div>
						
                        <div class=\"rodape-indicacao\">
                            <div class=\"fundo-indicar\">
                                <div class=\"item-mao\">
                                    <div class=\"mao-indica\"></div>
                                </div>

                                <div class=\"item-numero-indicacao\">
                                    <div class=\"text-numero-indicacao\">".$indicacoes."</div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"item-conteudo-resultados\">
                <div class=\"font-conteudo-resultados\">".$nome_empresa."</div>
                <div class=\"icone-localizacao endereco-localizacao\"></div>";
					for ($j = 0; $j < mysql_num_rows($result2); $j++){
						if (strlen($endereco[$j]) > 26){
							$endereco[$j] = substr($endereco[$j],0,26)."...";
						}
						$aux .= "<div class=\"parent item-endereco-localizacao\">
							<div class=\"child font-endereco-localizacao".($j+1)."\">".$endereco[$j]."</div>
						</div>";
					}
            $aux .= "</div>
        </div>";
                   
			$cont1++;
			$cont2++;
		}
		$aux .= "</div></a>";
	}
	echo $aux;
?>