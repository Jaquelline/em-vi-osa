<?php
	//Caso alguém que não seja o sistema tente acessar o script redireciona para a página inicial
	if (isset($_POST['num_de_empresas']) && isset($_POST['num_de_empresas'])){
		$num_de_empresas = $_POST['num_de_empresas'];
		$pagina = $_POST['pagina'];
	}
	else
		header("location:index.php");
		
	//Abre banco de dados
	require_once("../compartilhado/conexao.php");
	
	$offset = ($num_de_empresas*($pagina-1));
	
	//Busca empresas
	$query = "SELECT * FROM (SELECT * FROM viewtopservicos) AS resultado GROUP BY resultado.idCategoria ORDER BY resultado.numTotalIndicacoesPositivas DESC LIMIT $num_de_empresas OFFSET $offset";
	$result1 = mysql_query($query,$connection) or die(mysql_error());

	//Busca empresas da próxima página
	$aux = ($num_de_empresas*$pagina);
	$query = "SELECT * FROM (SELECT * FROM viewtopservicos) AS resultado GROUP BY resultado.idCategoria ORDER BY resultado.numTotalIndicacoesPositivas DESC LIMIT $num_de_empresas OFFSET $aux";
	$result2 = mysql_query($query,$connection) or die(mysql_error());
	
	//Atualiza o número de empresas com o número retornado pela consulta
	$num_de_empresas = mysql_num_rows($result1);
	
	$resposta = array();
	//Guarda número de empresas da próxima página
	$resposta[0] = mysql_num_rows($result2);
	
	$row = array();
	$i = 0;
	while ($row = mysql_fetch_assoc($result1)) {
		$resposta[$i+1] = $row["nomeEmpresa"];
		$resposta[$i+2] = $row["linkLogoEmpresa"];
		$resposta[$i+3] = $row["complementoLinkEmVicosa"];
		
		if ($row["numTotalIndicacoesPositivas"] == "")
			$resposta[$i+4] = 0;		
		else
			$resposta[$i+4] = $row["numTotalIndicacoesPositivas"];			
			
		$resposta[$i+5] = number_format($row["mediaAvaliacao"],1);		
		$i+=5;
	}
	
	echo json_encode($resposta);
?>