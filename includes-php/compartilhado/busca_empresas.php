<?php
	//Abre banco de dados
	require_once("conexao.php");
	
	$busca = "%";
	$busca .= $_POST['busca'];
	$busca .= "%";
	
	$string = $_POST['busca'];

	//Remove os conectivos iniciais
	for ($i = 0; $i < 5; $i++){
		$pattern = '/^( )*(a|e|o|da|de|do|para|os|as|um|uns|uma|umas|ao|aos|à|às|dos|das|dum|duns|duma|dumas|em|no|nos|na|nas|num|nuns|numa|numas|por|per|pelo|pelos|pela|pelas|ante|após|até|com|conforme|contra|consoante|desde|durante|em|exceto|entre|mediante|perante|por|salvo|sem|segundo|sob|sobre|trás)( )+/i';
		$replacement = '';
		$string =  preg_replace($pattern, $replacement, $string);
	}
	
	//Remove os conectivos do meio da palavra
	for ($i = 0; $i < 15; $i++){
		$pattern = '/( )+(a|e|o|da|de|do|para|os|as|um|uns|uma|umas|ao|aos|à|às|dos|das|dum|duns|duma|dumas|em|no|nos|na|nas|num|nuns|numa|numas|por|per|pelo|pelos|pela|pelas|ante|após|até|com|conforme|contra|consoante|desde|durante|em|exceto|entre|mediante|perante|por|salvo|sem|segundo|sob|sobre|trás)( )+/i';
		$replacement = ' ';
		$string =  preg_replace($pattern, $replacement, $string);
	}
	
	//Remove os conectivos finais
	for ($i = 0; $i < 5; $i++){
		$pattern = '/( )+(a|e|o|da|de|do|para|os|as|um|uns|uma|umas|ao|aos|à|às|dos|das|dum|duns|duma|dumas|em|no|nos|na|nas|num|nuns|numa|numas|por|per|pelo|pelos|pela|pelas|ante|após|até|com|conforme|contra|consoante|desde|durante|em|exceto|entre|mediante|perante|por|salvo|sem|segundo|sob|sobre|trás)( )*$/i';
		$replacement = '';
		$string =  preg_replace($pattern, $replacement, $string);
	}
	
	//Coloca % nos espaços em branco no início e final da frase
	$pattern = '/[[:space:]]/i';
	$replacement = '%';
	$string =  preg_replace($pattern, $replacement, $string);
	$string .= "%";
	$string = '%'.$string;
	
	//Busca empresas
	$query = "SELECT * 
	FROM (SELECT * FROM viewtopservicos) AS resultado 
	WHERE nomeEmpresa like '$busca' or nomeCategoria like '$busca' or palavrasChavesEmpresa like '$busca' or palavrasChavesEmpresa like '$string' 
	GROUP BY resultado.idCategoria 
	ORDER BY resultado.numTotalIndicacoesPositivas DESC LIMIT 8 OFFSET 0";
	$result1 = mysql_query($query,$connection) or die(mysql_error());
	
	$row = array();
	$resposta = array();
	$i = 0;
	while ($row = mysql_fetch_assoc($result1)) {
		$resposta[$i] = $row["nomeEmpresa"];
		$resposta[$i+1] = $row["complementoLinkEmVicosa"];
		$i+=2;
	}
	echo json_encode($resposta);
?>