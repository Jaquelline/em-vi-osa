jQuery(document).ready(function ($) {
    var id = '';

    /*Variaveis tamanho modal botões*/
    var width_modal_1 = 0;

    var height_modal_1 = 'auto';

    /*Variaveis tamanho modal slider*/
    var width_modal_2 = $( window ).width()*0.8;

    var height_modal_2 = width_modal_2*0.52;

    var img_width = width_modal_2-100;

    var img_height = img_width*0.52;

    var index_img = 0;

    $(function(){

        /*Centraliza avaliacao caso o estabelecimento não tenha facebook*/
        if($('.fb-page').is(':hidden')) {
            $('.item-fundo-avaliacao-detalhes').css({'float':'none'});
        }else{
            $('.item-fundo-avaliacao-detalhes').css({'float':'left'});
        }

        /*Define o tamanho das modais para cada tela*/
        if ($( window ).width() <= 600){
            width_modal_1 = 300;
        }

        if ($( window ).width() >= 601 && $( window ).width() <= 800){
            width_modal_1 = 320;
        }

        if ($( window ).width() >= 801 && $( window ).width() <= 1200){
            width_modal_1 = 400;
        }

        if ($( window ).width() >= 1201){
            width_modal_1 = 500;
        }
    });


    /*Define as configuracoes do slider externo para cada tela*/
   var slider1 = $('.item-slider .bxslider, .item-detalhes-slider-botoes .bxslider, #tela-celular-tablet .bxslider').bxSlider({
        mode: 'fade',
        auto: true,
        pause: 5000,
        captions: true,
        touchEnabled: true
    });


    /*Redimensiona cada modal a medida que a tela aumenta ou diminui de tamanho*/
    $( window ).resize(function() {

        if ($( window ).width() < 600){
            width_modal_1 = 300;
        }

        if ($( window ).width() >= 600 && $( window ).width() <= 800){
            width_modal_1 = 320;
        }

        if ($( window ).width() >= 801 && $( window ).width() <= 1200){
            width_modal_1 = 400;
        }

        if ($( window ).width() >= 1201){
            width_modal_1 = 500;
        }

        /*Altera a largura das modais*/
        $("#modal-telefone, #modal-hFuncionamento, #modal-fPagamento, #modal-sobre, #modal-planos-aceitos, #modal-num-avaliacao, #modal-mensagem").dialog( "option", "width", width_modal_1 );

        /*Recalcula o tamanho da modal slider*/
        width_modal_2 = $( window ).width()*0.8;

        height_modal_2 = width_modal_2*0.52;

        img_width = width_modal_2-100;

        img_height = img_width*0.52;

        $(".modal-slider").siblings('div.ui-widget-header').css({"background": 'none'});

        $('.modal-slider').height(height_modal_2);

        $('.modal-slider .bx-wrapper').width(width_modal_2);

        $(".modal-slider .bx-wrapper").css({
            "max-width": width_modal_2+"px !important"
        });

        $('.modal-slider .bx-wrapper').height(img_height);

        $('.modal-slider .bx-viewport').height(img_height);

        $(".modal-slider .bx-viewport").css({
            "width": img_width+"px"
        });

        $('.modal-slider .imagem-detalhes-zoom').height(img_height);

        $('.modal-slider .imagem-detalhes-zoom').width(img_width);

        $('.modal-slider .bx-pager').width(width_modal_2);

        $('.modal-slider .bxslider').width(img_width);

        /*Altera o tamanho da modal slider*/
        $(".modal-slider").dialog( "option", "width", width_modal_2 );

        $(".modal-slider").dialog( "option", "height", height_modal_2 );
    });

    /*Conexao com o facebook na desktop maior*/
   $(function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.4";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    /*Esta função trata a avalicao do estabelecimento com as estrelas
    * a classe icone-avaliacao-detalhes-amarelo-hover: quando passa o mouse a div fica amarela
    * a classe icone-avalicao-detalhes: quando não esta clicada e nem tem hover ela é cinza
    * a classe icone-avaliacao-detalhes-amarelo-clique é adicionada a div quando a div é clicada*/
    $(function(){

        $('#estrela-A').mouseenter(function(){

            if (id == ''){
                $("#eA").addClass('icone-avaliacao-detalhes-amarelo-hover');
            }else{
                if (id == 'eB'){
                    $("#eB").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eC'){
                    $("#eB, #eC").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }

                if (id == 'eD'){
                    $("#eB, #eC, #eD").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }

                if (id == 'eE'){
                    $("#eB, #eC, #eD, #eE").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }

            }

        });

        $('#estrela-A-maior').mouseenter(function(){

            if (id == ''){
                $("#eA-maior").addClass('icone-avaliacao-detalhes-amarelo-hover');
            }else{
                if (id == 'eB-maior'){
                    $("#eB-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eC-maior'){
                    $("#eB-maior, #eC-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }

                if (id == 'eD-maior'){
                    $("#eB-maior, #eC-maior, #eD-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }

                if (id == 'eE-maior'){
                    $("#eB-maior, #eC-maior, #eD-maior, #eE-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
            }
        });

        $('#estrela-A-menor').mouseenter(function(){

            if (id == ''){
                $("#eA-menor").addClass('icone-avaliacao-detalhes-amarelo-hover');
            }else{
                if (id == 'eB-menor'){
                    $("#eB-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eC-menor'){
                    $("#eB-menor, #eC-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }

                if (id == 'eD-menor'){
                    $("#eB-menor, #eC-menor, #eD-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }

                if (id == 'eE-menor'){
                    $("#eB-menor, #eC-menor, #eD-menor, #eE-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
            }
        });

        $('#estrela-A').mouseleave(function(){
            if (id == '' ){
                $('#eA').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
            }else{
                if (id == 'eB'){
                    $('#eB').addClass('icone-avaliacao-detalhes-amarelo-clique');
                }

                if (id == 'eC'){
                    $('#eB, #eC').addClass('icone-avaliacao-detalhes-amarelo-clique');
                }

                if (id == 'eD'){
                    $('#eB, #eC, #eD').addClass('icone-avaliacao-detalhes-amarelo-clique');
                }

                if (id == 'eE'){
                    $('#eB, #eC, #eD, #eE').addClass('icone-avaliacao-detalhes-amarelo-clique');
                }

            }

        });

        $('#estrela-A-maior').mouseleave(function(){
            if (id == '' ){
                $('#eA-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
            }else{
                if (id == 'eB-maior'){
                    $('#eB-maior').addClass('icone-avaliacao-detalhes-amarelo-clique');
                }

                if (id == 'eC-maior'){
                    $('#eB-maior, #eC-maior').addClass('icone-avaliacao-detalhes-amarelo-clique');
                }

                if (id == 'eD-maior'){
                    $('#eB-maior, #eC-maior, #eD-maior').addClass('icone-avaliacao-detalhes-amarelo-clique');
                }

                if (id == 'eE-maior'){
                    $('#eB-maior, #eC-maior, #eD-maior, #eE-maior').addClass('icone-avaliacao-detalhes-amarelo-clique');
                }
            }
        });

        $('#estrela-A-menor').mouseleave(function(){
            if (id == '' ){
                $('#eA-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
            }else{
                if (id == 'eB-menor'){
                    $('#eB-menor').addClass('icone-avaliacao-detalhes-amarelo-clique');
                }

                if (id == 'eC-menor'){
                    $('#eB-menor, #eC-menor').addClass('icone-avaliacao-detalhes-amarelo-clique');
                }

                if (id == 'eD-menor'){
                    $('#eB-menor, #eC-menor, #eD-menor').addClass('icone-avaliacao-detalhes-amarelo-clique');
                }

                if (id == 'eE-menor'){
                    $('#eB-menor, #eC-menor, #eD-menor, #eE-menor').addClass('icone-avaliacao-detalhes-amarelo-clique');
                }
            }
        });

         $('#estrela-B').mouseenter(function(){
            if (id == '') {
                 $("#eA, #eB").addClass('icone-avaliacao-detalhes-amarelo-hover');
             }else{
                if (id == 'eA'){
                    $("#eB").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                }
                if (id == 'eC'){
                    $("#eC").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eD'){
                    $("#eC, #eD").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eE'){
                    $("#eC, #eD, #eE").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }

             }

         });

        $('#estrela-B-maior').mouseenter(function(){
            if (id == '') {
                $("#eA-maior, #eB-maior").addClass('icone-avaliacao-detalhes-amarelo-hover');
            }else{
                if (id == 'eA-maior'){
                    $("#eB-maior").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                }
                if (id == 'eC-maior'){
                    $("#eC-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eD-maior'){
                    $("#eC-maior, #eD-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eE-maior'){
                    $("#eC-maior, #eD-maior, #eE-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
            }
        });

        $('#estrela-B-menor').mouseenter(function(){
            if (id == '') {
                $("#eA-menor, #eB-menorr").addClass('icone-avaliacao-detalhes-amarelo-hover');
            }else{
                if (id == 'eA-menor'){
                    $("#eB-menor").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                }
                if (id == 'eC-menor'){
                    $("#eC-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eD-menor'){
                    $("#eC-menor, #eD-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eE-menor'){
                    $("#eC-menor, #eD-menor, #eE-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
            }
        });

         $('#estrela-B').mouseleave(function(){
             if (id == '' ) {
                 $("#eA, #eB").removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
             }else{
                 if (id == 'eA'){
                     $('#eA, #eB').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                 }
                 if (id == 'eB'){
                     $('#eB').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                 }
                 if (id == 'eC'){
                     $('#eB, #eC').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                     $("#eC").addClass('icone-avaliacao-detalhes-amarelo-clique');
                 }

                 if (id == 'eD'){
                     $('#eB, #eC, #eD').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                     $("#eC, #eD").addClass('icone-avaliacao-detalhes-amarelo-clique');
                 }

                 if (id == 'eE'){
                     $('#eB, #eC, #eD, #eE').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                     $("#eC, #eD, #eE").addClass('icone-avaliacao-detalhes-amarelo-clique');
                 }

             }
         });

        $('#estrela-B-maior').mouseleave(function(){
            if (id == '' ) {
                $("#eA-maior, #eB-maior").removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
            }else{
                if (id == 'eA-maior'){
                    $('#eA-maior, #eB-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eB-maior'){
                    $('#eB-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eC-maior'){
                    $('#eB-maior, #eC-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                    $("#eC-maior").addClass('icone-avaliacao-detalhes-amarelo-clique');
                }

                if (id == 'eD-maior'){
                    $('#eB-maior, #eC-maior, #eD-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                    $("#eC-maior, #eD-maior").addClass('icone-avaliacao-detalhes-amarelo-clique');
                }

                if (id == 'eE-maior'){
                    $('#eB-maior, #eC-maior, #eD-maior, #eE-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                    $("#eC-maior, #eD-maior, #eE-maior").addClass('icone-avaliacao-detalhes-amarelo-clique');
                }
            }
        });

        $('#estrela-B-menor').mouseleave(function(){
            if (id == '' ) {
                $("#eA-menor, #eB-menor").removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
            }else{
                if (id == 'eA-menor'){
                    $('#eA-menor, #eB-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eB-menor'){
                    $('#eB-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eC-menor'){
                    $('#eB-menor, #eC-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                    $("#eC-menor").addClass('icone-avaliacao-detalhes-amarelo-clique');
                }

                if (id == 'eD-menor'){
                    $('#eB-menor, #eC-menor, #eD-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                    $("#eC-menor, #eD-menor").addClass('icone-avaliacao-detalhes-amarelo-clique');
                }

                if (id == 'eE-menor'){
                    $('#eB-menor, #eC-menor, #eD-menor, #eE-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                    $("#eC-menor, #eD-menor, #eE-menor").addClass('icone-avaliacao-detalhes-amarelo-clique');
                }
            }
        });

        $('#estrela-C').mouseenter(function(){
            if (id == '') {
                $("#eA, #eB, #eC").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
            }else{
                if (id == 'eA'){
                    $("#eB, #eC").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                }
                if (id == 'eB'){
                    $("#eC").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                }
                if (id == 'eD'){
                    $("#eD").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eE'){
                    $("#eD, #eE").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }

            }
        });

        $('#estrela-C-maior').mouseenter(function(){
            if (id == '') {
                $("#eA-maior, #eB-maior, #eC-maior").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
            }else{
                if (id == 'eA-maior'){
                    $("#eB-maior, #eC-maior").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                }
                if (id == 'eB-maior'){
                    $("#eC-maior").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                }
                if (id == 'eD-maior'){
                    $("#eD-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eE-maior'){
                    $("#eD-maior, #eE-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }

            }
        });

        $('#estrela-C-menor').mouseenter(function(){
            if (id == '') {
                $("#eA-menor, #eB-menor, #eC-menor").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
            }else{
                if (id == 'eA-menor'){
                    $("#eB-menor, #eC-menor").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                }
                if (id == 'eB-menor'){
                    $("#eC-menor").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                }
                if (id == 'eD-menor'){
                    $("#eD-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eE-menor'){
                    $("#eD-menor, #eE-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }

            }
        });

        $('#estrela-C').mouseleave(function(){
            if (id == '') {
                $('#eA, #eB, #eC').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
            }else{
                if (id == 'eA'){
                    $('#eA, #eB, #eC').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eB'){
                    $('#eA, #eB, #eC').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eC'){
                    $('#eC').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eD'){
                    $('#eA, #eB, #eC, #eD').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                    $("#eD").addClass('icone-avaliacao-detalhes-amarelo-clique');
                }
                if (id == 'eE'){
                    $('#eA, #eB, #eC, #eD, #eE').removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                    $("#eD, #eE").addClass('icone-avaliacao-detalhes-amarelo-clique');
                }
            }
        });

        $('#estrela-C-maior').mouseleave(function(){
            if (id == '') {
                $('#eA-maior, #eB-maior, #eC-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
            }else{
                if (id == 'eA-maior'){
                    $('#eA-maior, #eB-maior, #eC-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eB-maior'){
                    $('#eA-maior, #eB-maior, #eC-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eC-maior'){
                    $('#eC-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eD-maior'){
                    $('#eA-maior, #eB-maior, #eC-maior, #eD-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                    $("#eD-maior").addClass('icone-avaliacao-detalhes-amarelo-clique');
                }
                if (id == 'eE-maior'){
                    $('#eA-maior, #eB-maior, #eC-maior, #eD-maior, #eE-maior').removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                    $("#eD-maior, #eE-maior").addClass('icone-avaliacao-detalhes-amarelo-clique');
                }
            }
        });

        $('#estrela-C-menor').mouseleave(function(){
            if (id == '') {
                $('#eA-menor, #eB-menor, #eC-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
            }else{
                if (id == 'eA-menor'){
                    $('#eA-menor, #eB-menor, #eC-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eB-menor'){
                    $('#eA-menor, #eB-menor, #eC-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eC-menor'){
                    $('#eC-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eD-menor'){
                    $('#eA-menor, #eB-menor, #eC-menor, #eD-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                    $("#eD-menor").addClass('icone-avaliacao-detalhes-amarelo-clique');
                }
                if (id == 'eE-menor'){
                    $('#eA-menor, #eB-menor, #eC-menor, #eD-menor, #eE-menor').removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                    $("#eD-menor, #eE-menor").addClass('icone-avaliacao-detalhes-amarelo-clique');
                }
            }
        });

         $('#estrela-D').mouseenter(function(){
            if (id == '') {
                 $("#eA, #eB, #eC, #eD").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
             }else{
                if (id == 'eA'){
                    $("#eB, #eC, #eD").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                }
                if (id == 'eB'){
                    $("#eC, #eD").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                }
                if (id == 'eC'){
                    $("#eD").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                }
                if (id == 'eE') {
                    $("#eE").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }

             }
         });

        $('#estrela-D-maior').mouseenter(function(){
            if (id == '') {
                $("#eA-maior, #eB-maior, #eC-maior, #eD-maior").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
            }else{
                if (id == 'eA-maior'){
                    $("#eB-maior, #eC-maior, #eD-maior").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                }
                if (id == 'eB-maior'){
                    $("#eC-maior, #eD-maior").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                }
                if (id == 'eC-maior'){
                    $("#eD-maior").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                }
                if (id == 'eE-maior') {
                    $("#eE-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
            }
        });

        $('#estrela-D-menor').mouseenter(function(){
            if (id == '') {
                $("#eA-menor, #eB-menor, #eC-menor, #eD-menor").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
            }else{
                if (id == 'eA-menor'){
                    $("#eB-menor, #eC-menor, #eD-menor").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                }
                if (id == 'eB-menor'){
                    $("#eC-menor, #eD-menor").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                }
                if (id == 'eC-menor'){
                    $("#eD-menor").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');
                }
                if (id == 'eE-menor') {
                    $("#eE-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
            }
        });

        $('#estrela-D').mouseleave(function(){
            if (id == '') {
                $('#eA, #eB, #eC, #eD').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
            }else{
                if (id == 'eA'){
                    $('#eA, #eB, #eC, #eD').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eB'){
                    $('#eA, #eB, #eC, #eD').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eC'){
                    $('#eA, #eB, #eC, #eD').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eD'){
                    $('#eD').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eE'){
                    $('#eA, #eB, #eC, #eD, #eE').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes-amarelo-hover');
                    $("#eD").addClass('icone-avaliacao-detalhes-amarelo-clique');
                }
            }
        });

        $('#estrela-D-maior').mouseleave(function(){
            if (id == '') {
                $('#eA-maior, #eB-maior, #eC-maior, #eD-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
            }else{
                if (id == 'eA-maior'){
                    $('#eA-maior, #eB-maior, #eC-maior, #eD-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eB-maior'){
                    $('#eA-maior, #eB-maior, #eC-maior, #eD-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eC-maior'){
                    $('#eA-maior, #eB-maior, #eC-maior, #eD-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eD-maior'){
                    $('#eD-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eE-maior'){
                    $('#eA-maior, #eB-maior, #eC-maior, #eD-maior, #eE-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes-amarelo-hover');
                    $("#eD-maior").addClass('icone-avaliacao-detalhes-amarelo-clique');
                }
            }
        });

        $('#estrela-D-menor').mouseleave(function(){
            if (id == '') {
                $('#eA-menor, #eB-menor, #eC-menor, #eD-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
            }else{
                if (id == 'eA-menor'){
                    $('#eA-menor, #eB-menor, #eC-menor, #eD-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eB-menor'){
                    $('#eA-menor, #eB-menor, #eC-menor, #eD-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eC-menor'){
                    $('#eA-menor, #eB-menor, #eC-menor, #eD-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eD-menor'){
                    $('#eD-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eE-menor'){
                    $('#eA-menor, #eB-menor, #eC-menor, #eD-menor, #eE-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes-amarelo-hover');
                    $("#eD-menor").addClass('icone-avaliacao-detalhes-amarelo-clique');
                }
            }
        });

        $('#estrela-E').mouseenter(function(){

            $("#eA, #eB, #eC, #eD, #eE").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');

        });

        $('#estrela-E-maior').mouseenter(function(){

            $("#eA-maior, #eB-maior, #eC-maior, #eD-maior, #eE-maior").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');

        });

        $('#estrela-E-menor').mouseenter(function(){

            $("#eA-menor, #eB-menor, #eC-menor, #eD-menor, #eE-menor").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-hover');

        });

        $('#estrela-E').mouseleave(function(){
            if (id == '') {
                $('#eA, #eB, #eC, #eD, #eE').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
            }else{
                if (id == 'eA'){
                    $('#eA, #eB, #eC, #eD, #eE').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eB'){
                    $('#eA, #eB, #eC, #eD, #eE').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eC'){
                    $('#eA, #eB, #eC, #eD, #eE').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eD'){
                    $('#eA, #eB, #eC, #eD, #eE').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eE'){
                    $('#eE').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes-amarelo-clique');
                }
            }
        });

        $('#estrela-E-maior').mouseleave(function(){
            if (id == '') {
                $('#eA-maior, #eB-maior, #eC-maior, #eD-maior, #eE-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
            }else{
                if (id == 'eA-maior'){
                    $('#eA-maior, #eB-maior, #eC-maior, #eD-maior, #eE-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eB-maior'){
                    $('#eA-maior, #eB-maior, #eC-maior, #eD-maior, #eE-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eC-maior'){
                    $('#eA-maior, #eB-maior, #eC-maior, #eD-maior, #eE-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eD-maior'){
                    $('#eA-maior, #eB-maior, #eC-maior, #eD-maior, #eE-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eE-maior'){
                    $('#eE-maior').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes-amarelo-clique');
                }
            }
        });

        $('#estrela-E-menor').mouseleave(function(){
            if (id == '') {
                $('#eA-menor, #eB-menor, #eC-menor, #eD-menor, #eE-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
            }else{
                if (id == 'eA-menor'){
                    $('#eA-menor, #eB-menor, #eC-menor, #eD-menor, #eE-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eB-menor'){
                    $('#eA-menor, #eB-menor, #eC-menor, #eD-menor, #eE-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eC-menor'){
                    $('#eA-menor, #eB-menor, #eC-menor, #eD-menor, #eE-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eD-menor'){
                    $('#eA-menor, #eB-menor, #eC-menor, #eD-menor, #eE-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes');
                }
                if (id == 'eE-menor'){
                    $('#eE-menor').removeClass('icone-avaliacao-detalhes-amarelo-hover').addClass('icone-avaliacao-detalhes-amarelo-clique');
                }
            }
        });
    });

    $(function(){
        $('#estrela-A').click(function(){
            if (id == ''){
                id = 'eA';
                $("#eA").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eB'){
                id = 'eA';
                $("#eB").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eC'){
                id = 'eA';
                $("#eB, #eC").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eD'){
                id = 'eA';
                $("#eB, #eC, #eD").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eE'){
                id = 'eA';
                $("#eB, #eC, #eD, #eE").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }

        });

        $('#estrela-A-maior').click(function(){
            if (id == ''){
                id = 'eA-maior';
                $("#eA-maior").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eB-maior'){
                id = 'eA-maior';
                $("#eB-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-maior").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eC-maior'){
                id = 'eA-maior';
                $("#eB-maior, #eC-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-maior").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eD-maior'){
                id = 'eA-maior';
                $("#eB-maior, #eC-maior, #eD-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-maior").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eE-maior'){
                id = 'eA-maior';
                $("#eB-maior, #eC-maior, #eD-maior, #eE-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-maior").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }

        });

        /*Quando clica na estrela atribui a classe icone-avaliacao-detalhes-amarelo-clique para todas as
        * estrela anteriores a estrela clicada*/
        $('#estrela-A-menor').click(function(){
            if (id == ''){
                id = 'eA-menor';
                $("#eA-menor").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eB-menor'){
                id = 'eA-menor';
                $("#eB-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-menor").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eC-menor'){
                id = 'eA-menor';
                $("#eB-menor, #eC-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-menor").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eD-menor'){
                id = 'eA-menor';
                $("#eB-menor, #eC-menor, #eD-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-menor").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eE-menor'){
                id = 'eA-menor';
                $("#eB-menor, #eC-menor, #eD-menor, #eE-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-menor").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }

        });

        $('#estrela-B').click(function(){
            if (id == '') {
                $("#eA, #eB").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
                id = 'eB';
            }
            if (id == 'eA'){
                id = 'eB';
                $("#eA, #eB").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eC'){
                id = 'eB';
                $("#eC").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA, #eB").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eD'){
                id = 'eB';
                $("#eC, #eD").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA, #eB").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eE'){
                id = 'eB';
                $("#eC, #eD, #eE").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA, #eB").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }

        });

        $('#estrela-B-maior').click(function(){
            if (id == '') {
                $("#eA-maior, #eB-maior").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
                id = 'eB-maior';
            }
            if (id == 'eA-maior'){
                id = 'eB-maior';
                $("#eA-maior, #eB-maior").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eC-maior'){
                id = 'eB-maior';
                $("#eC-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-maior, #eB-maior").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eD-maior'){
                id = 'eB-maior';
                $("#eC-maior, #eD-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-maior, #eB-maior").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eE-maior'){
                id = 'eB-maior';
                $("#eC-maior, #eD-maior, #eE-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-maior, #eB-maior").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }

        });

        $('#estrela-B-menor').click(function(){
            if (id == '') {
                $("#eA-menor, #eB-menor").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
                id = 'eB-menor';
            }
            if (id == 'eA-menor'){
                id = 'eB-menor';
                $("#eA-menor, #eB-menor").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eC-menor'){
                id = 'eB-menor';
                $("#eC-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-menor, #eB-menor").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eD-menor'){
                id = 'eB-menor';
                $("#eC-menor, #eD-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-menor, #eB-menor").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eE-menor'){
                id = 'eB-menor';
                $("#eC-menor, #eD-menor, #eE-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-menor, #eB-menor").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }

        });

        $('#estrela-C').click(function(){
            if (id == '') {
                $("#eA, #eB, #eC").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
                id = 'eC';
            }
            if (id == 'eA'){
                id = 'eC';
                $("#eA, #eB, #eC").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eB'){
                id = 'eC';
                $("#eA, #eB, #eC").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eD'){
                id = 'eC';
                $("#eD").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA, #eB, #eC").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eE'){
                id = 'eC';
                $("#eD, #eE").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA, #eB, #eC").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }

        });

        $('#estrela-C-maior').click(function(){
            if (id == '') {
                $("#eA-maior, #eB-maior, #eC-maior").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
                id = 'eC-maior';
            }
            if (id == 'eA-maior'){
                id = 'eC-maior';
                $("#eA-maior, #eB-maior, #eC-maior").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eB-maior'){
                id = 'eC-maior';
                $("#eA-maior, #eB-maior, #eC-maior").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eD-maior'){
                id = 'eC-maior';
                $("#eD-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-maior, #eB-maior, #eC-maior").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eE-maior'){
                id = 'eC-maior';
                $("#eD-maior, #eE-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-maior, #eB-maior, #eC-maior").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }

        });

        $('#estrela-C-menor').click(function(){
            if (id == '') {
                $("#eA-menor, #eB-menor, #eC-menor").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
                id = 'eC-menor';
            }
            if (id == 'eA-menor'){
                id = 'eC-menor';
                $("#eA-menor, #eB-menor, #eC-menor").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eB-menor'){
                id = 'eC-menor';
                $("#eA-menor, #eB-menor, #eC-menor").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eD-menor'){
                id = 'eC-menor';
                $("#eD-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-menor, #eB-menor, #eC-menor").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eE-menor'){
                id = 'eC-menor';
                $("#eD-menor, #eE-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-menor, #eB-menor, #eC-menor").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }

        });

        $('#estrela-D').click(function(){
            if (id == '') {
                $("#eA, #eB, #eC, #eD").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
                id = 'eD';
            }
            if (id == 'eA'){
                id = 'eD';
                $("#eA, #eB, #eC, #eD").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eB'){
                id = 'eD';
                $("#eA, #eB, #eC, #eD").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eC'){
                id = 'eD';
                $("#eA, #eB, #eC, #eD").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eE'){
                id = 'eD';
                $("#eE").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA, #eB, #eC, #eD").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }

        });

        $('#estrela-D-maior').click(function(){
            if (id == '') {
                $("#eA-maior, #eB-maior, #eC-maior, #eD-maior").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
                id = 'eD-maior';
            }
            if (id == 'eA-maior'){
                id = 'eD-maior';
                $("#eA-maior, #eB-maior, #eC-maior, #eD-maior").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eB-maior'){
                id = 'eD-maior';
                $("#eA-maior, #eB-maior, #eC-maior, #eD-maior").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eC-maior'){
                id = 'eD-maior';
                $("#eA-maior, #eB-maior, #eC-maior, #eD-maior").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eE-maior'){
                id = 'eD-maior';
                $("#eE-maior").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-maior, #eB-maior, #eC-maior, #eD-maior").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }

        });

        $('#estrela-D-menor').click(function(){
            if (id == '') {
                $("#eA-menor, #eB-menor, #eC-menor, #eD-menor").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
                id = 'eD-menor';
            }
            if (id == 'eA-menor'){
                id = 'eD-menor';
                $("#eA-menor, #eB-menor, #eC-menor, #eD-menor").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eB-menor'){
                id = 'eD-menor';
                $("#eA-menor, #eB-menor, #eC-menor, #eD-menor").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eC-menor'){
                id = 'eD-menor';
                $("#eA-menor, #eB-menor, #eC-menor, #eD-menor").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
            }
            if (id == 'eE-menor'){
                id = 'eD-menor';
                $("#eE-menor").removeClass('icone-avaliacao-detalhes-amarelo-clique');
                $("#eA-menor, #eB-menor, #eC-menor, #eD-menor").addClass('icone-avaliacao-detalhes-amarelo-clique');
            }

        });

        $('#estrela-E').click(function(){
            $("#eA, #eB, #eC, #eD, #eE").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
            id = 'eE';
        });

        $('#estrela-E-maior').click(function(){
            $("#eA-maior, #eB-maior, #eC-maior, #eD-maior, #eE-maior").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
            id = 'eE-maior';
        });

        $('#estrela-E-menor').click(function(){
            $("#eA-menor, #eB-menor, #eC-menor, #eD-menor, #eE-menor").removeClass('icone-avaliacao-detalhes').addClass('icone-avaliacao-detalhes-amarelo-clique');
            id = 'eE-menor';
        });

    });


    /*Configuracoes das dialogs dos botoes*/
    $(function() {

        $( "#modal-telefone, #modal-hFuncionamento, #modal-fPagamento, #modal-sobre, #modal-planos-aceitos, #modal-num-avaliacao" ).dialog({
            open: function() {
                jQuery('.ui-widget-overlay').bind('click', function() {
                    jQuery( "#modal-telefone, #modal-hFuncionamento, #modal-fPagamento, #modal-sobre, #modal-planos-aceitos, #modal-num-avaliacao").dialog('close');
                });

                //$('.ui-dialog-titlebar').parent().focus();

                //$('#modal-hFuncionamento').focus();
                //$('#modal-telefone, #modal-hFuncionamento, #modal-fPagamento, #modal-sobre, #modal-planos-aceitos, #modal-num-avaliacao').scrollTop(0);

            },
            autoOpen: false,
            modal: true,
            width: width_modal_1,
            height: height_modal_1,
            buttons: {
                Voltar: function() {
                    $( this ).dialog( "close" );
                }
            }
        });

        $( "#modal-mensagem" ).dialog({
            autoOpen: false,
            modal: true,
            width: width_modal_1,
            height: height_modal_1,
            open: function() {
                $( this ).find( "[type=submit]" ).hide();

                jQuery('.ui-widget-overlay').bind('click', function() {
                    jQuery('#modal-mensagem').dialog('close');
                    $("#msgErro-detalhes").empty();
                    $("#assuntoid, #mensagem, #nomeid-detalhes, #emailid-detalhes").val("");
                })

            },
            buttons: [
                {
                    id: "ev",
                    text: "Enviar",
                    click: function () {

                        if ($("#nomeid-detalhes").val().length === 0){
                            $("#msgErro-detalhes").empty();
                            $("#msgErro-detalhes").text("Preencha o campo nome!");
                        }else{
                            if ($("#emailid-detalhes").val().length === 0){
                                $("#msgErro-detalhes").empty();
                                $("#msgErro-detalhes").text("Preencha o campo email!");
                            }else{
                                if(!isValidEmailAddress($("#emailid-detalhes").val())){

                                    $("#msgErro-detalhes").empty();
                                    $("#msgErro-detalhes").text("Email inválido!");
                                }else{
                                    if ($("#assuntoid").val().length === 0){
                                        $("#msgErro-detalhes").empty();
                                        $("#msgErro-detalhes").text("Preencha o campo assunto!");
                                    }else{
                                        if ($("#mensagem").val().length === 0){
                                            $("#msgErro-detalhes").empty();
                                            $("#msgErro-detalhes").text("Preencha o campo mensagem!");
                                        }
                                    }

                                }

                            }
                        }

                    },
                    type: "submit",
                    form: "form-contato-comercio" // <-- Make the association
                },
                {
                    text: "Voltar",
                    click: function() {
                        $("#msgErro-detalhes").empty();
                        $("#assuntoid, #mensagem, #nomeid-detalhes, #emailid-detalhes").val("");
                        $( this ).dialog( "close" );
                    }
                }
            ]
        });

        /*Configuracao modal slider interno*/
        $( ".modal-slider" ).dialog({
            open: function() {
                jQuery('.ui-widget-overlay').bind('click', function() {
                    jQuery('.modal-slider').dialog('close');
                })
            },
            autoOpen: false,
            modal: true,
            width: width_modal_2,
            height: height_modal_2,
            touchEnabled: true
        });

    });

    /*Clica na imagem com zoom do slider e fecha a dialog*/
    $('.modal-slider .imagem-detalhes-zoom').on('click', function(){
        $('.modal-slider').dialog('close');
    });


    /*Clica nos botoes e abre as dialog*/
    $(".item-botao-telefone" ).click(function() {
        $("#modal-telefone" ).dialog( "open" );
    });

    $( "#item-hFuncionamento" ).click(function() {
        $("#modal-hFuncionamento" ).dialog( "open" );
    });

    $( "#item-hFuncionamento-dMenor" ).click(function() {
        $("#modal-hFuncionamento" ).dialog( "open" );
    });

    $( "#item-hFuncionamento-dMaior" ).click(function() {
        $("#modal-hFuncionamento" ).dialog( "open" );
    });

    $("#item-mensagem" ).click(function() {
        $("#modal-mensagem" ).dialog( "open" );
    });

    $("#item-mensagem-dMenor" ).click(function() {
        $("#modal-mensagem" ).dialog( "open" );
    });

    $("#item-mensagem-dMaior" ).click(function() {
        $("#modal-mensagem" ).dialog( "open" );
    });

    $("#item-fPagamento" ).click(function() {
        $("#modal-fPagamento" ).dialog( "open" );
    });

    $("#item-fPagamento-dMenor" ).click(function() {
        $("#modal-fPagamento" ).dialog( "open" );
    });

    $("#item-fPagamento-dMaior" ).click(function() {
        $("#modal-fPagamento" ).dialog( "open" );
    });

    $("#item-sobre" ).click(function() {
        $("#modal-sobre" ).dialog( "open" );
    });

    $("#item-sobre-dMenor" ).click(function() {
        $("#modal-sobre" ).dialog( "open" );
    });

    $("#item-sobre-dMaior" ).click(function() {
        $("#modal-sobre" ).dialog( "open" );
    });

    $("#item-planos" ).click(function() {
        $("#modal-planos-aceitos" ).dialog( "open" );
    });

    $("#item-planos-dMenor" ).click(function() {
        $("#modal-planos-aceitos" ).dialog( "open" );
    });

    $("#item-planos-dMaior" ).click(function() {
        $("#modal-planos-aceitos" ).dialog( "open" );
    });

    $("#item-num-avaliacao" ).click(function() {
        $("#modal-num-avaliacao" ).dialog( "open" );
    });

    $("#item-num-avaliacao-menor" ).click(function() {
        $("#modal-num-avaliacao" ).dialog( "open" );
    });

    $("#item-num-avaliacao-maior" ).click(function() {
        $("#modal-num-avaliacao" ).dialog( "open" );
    });

    /*Clica na imagem do slider externo, calcula o tamanho da modal interna e abre a modal*/
    $(".imagem-detalhes").click(index_img,function() {

       index_img = parseInt(this.id);

        slider2.goToSlide(index_img);

        $(".modal-slider").siblings('div.ui-widget-header').css({"background": 'none'});

        $('.modal-slider').height(height_modal_2);

        $('.modal-slider .bx-wrapper').width(width_modal_2);

        $(".modal-slider .bx-wrapper").css({
            "max-width": width_modal_2+"px !important"
        });

        $('.modal-slider .bx-wrapper').height(img_height);

        $('.modal-slider .bx-viewport').height(img_height);

        $(".modal-slider .bx-viewport").css({
            "width": img_width+"px"
        });

        //$('.modal-slider ul li').width(img_width);

        $('.modal-slider .imagem-detalhes-zoom').height(img_height);

        $('.modal-slider .imagem-detalhes-zoom').width(img_width);

        $('.modal-slider .bx-pager').width(width_modal_2);

        $(".modal-slider" ).dialog( "open" );
    });


    var slider2 = $('.modal-slider .bxslider').bxSlider({
        startSlide: 0,
        mode: 'fade',
        slideWidth: img_width,
        captions: true,
        touchEnabled: true
    });
});

//NOVAS FUNÇÕES A PARTIR DAQUI

function indico_d_maior(complemento_link){
	//Verifica se o usuário está logado no facebook
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			FB.api(response.authResponse.userID, {fields: 'link,email,name,picture'}, function(response) {
				console.log(response);	
				var id_usuario = response.id;
				var nome_usuario = response.name;
				var link_perfil = response.link;
				var email_usuario = response.email;
				var link_foto = response.picture.data.url;
				
				$.post("includes-php/detalhes/indica_empresa.php",{id_usuario: id_usuario, nome_usuario: nome_usuario, link_foto: link_foto, link_perfil: link_perfil, email_usuario: email_usuario, complemento_link: complemento_link},
						function (data){
								$("#indicacao-icone-d-maior").hide();
								$("#botao-indicacao-d-maior").hide();
								$("#indicacao-d-maior").show();
						});
				});
			}
		else{
			//Caso não esteja logado faz login
			FB.login(function(response) {
				if (response.status === 'connected'){
					FB.api(response.authResponse.userID, {fields: 'link,email,name,picture'}, function(response) {
						var id_usuario = response.id;
						var nome_usuario = response.name;
						var link_perfil = response.link;
						var email_usuario = response.email;
						var link_foto = response.picture.data.url;
						
						$.post("includes-php/detalhes/indica_empresa.php",{id_usuario: id_usuario, nome_usuario: nome_usuario, link_foto: link_foto, link_perfil: link_perfil, email_usuario: email_usuario, complemento_link: complemento_link},
							function (data){
								$("#indicacao-icone-d-maior").hide();
								$("#botao-indicacao-d-maior").hide();
								$("#indicacao-d-maior").show();
							});
						});
				}
			}, {scope: 'email,user_photos,public_profile'});
		}
	});
};

function indico_d_menor(complemento_link){
	//Verifica se o usuário está logado no facebook
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			FB.api(response.authResponse.userID, {fields: 'link,email,name,picture'}, function(response) {
				var id_usuario = response.id;
				var nome_usuario = response.name;
				var link_perfil = response.link;
				var email_usuario = response.email;
				var link_foto = response.picture.data.url;
						
				$.post("includes-php/detalhes/indica_empresa.php",{id_usuario: id_usuario, nome_usuario: nome_usuario, link_foto: link_foto, link_perfil: link_perfil, email_usuario: email_usuario, complemento_link: complemento_link},
						function (data){
								$("#indicacao-icone-d-menor").hide();
								$("#botao-indicacao-d-menor").hide();
								$("#indicacao-d-menor").show();
						});
				});
			}
		else{
			//Caso não esteja logado faz login
			FB.login(function(response) {
				if (response.status === 'connected'){
					FB.api(response.authResponse.userID, {fields: 'link,email,name,picture'}, function(response) {
						var id_usuario = response.id;
						var nome_usuario = response.name;
						var link_perfil = response.link;
						var email_usuario = response.email;
						var link_foto = response.picture.data.url;
						
						$.post("includes-php/detalhes/indica_empresa.php",{id_usuario: id_usuario, nome_usuario: nome_usuario, link_foto: link_foto, link_perfil: link_perfil, email_usuario: email_usuario, complemento_link: complemento_link},
							function (data){
								$("#indicacao-icone-d-menor").hide();
								$("#botao-indicacao-d-menor").hide();
								$("#indicacao-d-menor").show();
							});
						});
				}
			}, {scope: 'email,user_photos,public_profile'});
		}
	});
};

function indico_tablet(complemento_link){
	//Verifica se o usuário está logado no facebook
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			FB.api(response.authResponse.userID, {fields: 'link,email,name,picture'}, function(response) {
				var id_usuario = response.id;
				var nome_usuario = response.name;
				var link_perfil = response.link;
				var email_usuario = response.email;
				var link_foto = response.picture.data.url;
						
				$.post("includes-php/detalhes/indica_empresa.php",{id_usuario: id_usuario, nome_usuario: nome_usuario, link_foto: link_foto, link_perfil: link_perfil, email_usuario: email_usuario, complemento_link: complemento_link},
						function (data){
								$("#indicacao-icone-tablet").hide();
								$("#botao-indicacao-tablet").hide();
								$("#indicacao-tablet").show();
						});
				});
			}
		else{
			//Caso não esteja logado faz login
			FB.login(function(response) {
				if (response.status === 'connected'){
					FB.api(response.authResponse.userID, {fields: 'link,email,name,picture'}, function(response) {
						var id_usuario = response.id;
						var nome_usuario = response.name;
						var link_perfil = response.link;
						var email_usuario = response.email;
						var link_foto = response.picture.data.url;
						
						$.post("includes-php/detalhes/indica_empresa.php",{id_usuario: id_usuario, nome_usuario: nome_usuario, link_foto: link_foto, link_perfil: link_perfil, email_usuario: email_usuario, complemento_link: complemento_link},
							function (data){
								$("#indicacao-icone-tablet").hide();
								$("#botao-indicacao-tablet").hide();
								$("#indicacao-tablet").show();
							});
						});
				}
			}, {scope: 'email,user_photos,public_profile'});
		}
	});
};

function avaliar_detalhes_d_maior(complemento_link){
	var nota = 0;
	//Pega nota (estrelas)
	if ($('#eA-maior').attr('class') != "icone-avaliacao-detalhes")
		nota++;
	if ($('#eB-maior').attr('class') != "icone-avaliacao-detalhes")
		nota++;
	if ($('#eC-maior').attr('class') != "icone-avaliacao-detalhes")
		nota++;
	if ($('#eD-maior').attr('class') != "icone-avaliacao-detalhes")
		nota++;
	if ($('#eE-maior').attr('class') != "icone-avaliacao-detalhes")
		nota++;
			
	//Pega Mensagem
	var mensagem = $('textarea#fundo-mensagem-detalhes-d-maior').val();
			
	//Verifica se o usuário está logado no facebook
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			FB.api(response.authResponse.userID, {fields: 'link,email,name,picture'}, function(response) {
				var id_usuario = response.id;
				var nome_usuario = response.name;
				var link_perfil = response.link;
				var email_usuario = response.email;
				var link_foto = response.picture.data.url;
						
				$.post("includes-php/detalhes/avalia_empresa.php",{id_usuario: id_usuario, nome_usuario: nome_usuario, link_foto: link_foto, link_perfil: link_perfil, email_usuario: email_usuario, complemento_link: complemento_link, mensagem: mensagem, nota: nota},
						function (data){
							$("#avaliacao-facebook-d-maior").html("<div style = \"font-family: 'Myriad Pro', 'Gill Sans', 'Gill Sans MT', Calibri, sans-serif; color: #F5911E; text-align:center;\"></br><p style = 'font-size: 35px;'>Muito obrigado!</p> <p style = 'font-size: 30px;'>Sua avaliação está sujeita a aprovação de um moderador.</p></div>");
						});
				});
			}
		else{
			//Caso não esteja logado faz login
			FB.login(function(response) {
				if (response.status === 'connected'){
					FB.api(response.authResponse.userID, {fields: 'link,email,name,picture'}, function(response) {
						var id_usuario = response.id;
						var nome_usuario = response.name;
						var link_perfil = response.link;
						var email_usuario = response.email;
						var link_foto = response.picture.data.url;
						
						$.post("includes-php/detalhes/avalia_empresa.php",{id_usuario: id_usuario, nome_usuario: nome_usuario, link_foto: link_foto, link_perfil: link_perfil, email_usuario: email_usuario, complemento_link: complemento_link, mensagem: mensagem, nota: nota},
							function (data){
								$("#avaliacao-facebook-d-maior").html("<div style = \"font-family: 'Myriad Pro', 'Gill Sans', 'Gill Sans MT', Calibri, sans-serif; color: #F5911E; text-align:center;\"></br><p style = 'font-size: 35px;'>Muito obrigado!</p> <p style = 'font-size: 30px;'>Sua avaliação está sujeita a aprovação de um moderador.</p></div>");
							});
						});
				}
			}, {scope: 'email,user_photos,public_profile'});
		}
	});
};

function avaliar_detalhes_d_menor(complemento_link){
	var nota = 0;
	//Pega nota (estrelas)
	if ($('#eA-menor').attr('class') != "icone-avaliacao-detalhes")
		nota++;
	if ($('#eB-menor').attr('class') != "icone-avaliacao-detalhes")
		nota++;
	if ($('#eC-menor').attr('class') != "icone-avaliacao-detalhes")
		nota++;
	if ($('#eD-menor').attr('class') != "icone-avaliacao-detalhes")
		nota++;
	if ($('#eE-menor').attr('class') != "icone-avaliacao-detalhes")
		nota++;
			
	//Pega Mensagem
	var mensagem = $('textarea#fundo-mensagem-detalhes-d-menor').val();
			
	//Verifica se o usuário está logado no facebook
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			FB.api(response.authResponse.userID, {fields: 'link,email,name,picture'}, function(response) {
				var id_usuario = response.id;
				var nome_usuario = response.name;
				var link_perfil = response.link;
				var email_usuario = response.email;
				var link_foto = response.picture.data.url;
						
				$.post("includes-php/detalhes/avalia_empresa.php",{id_usuario: id_usuario, nome_usuario: nome_usuario, link_foto: link_foto, link_perfil: link_perfil, email_usuario: email_usuario, complemento_link: complemento_link, mensagem: mensagem, nota: nota},
						function (data){
							$("#avaliacao-facebook-d-menor").html("<div style = \"font-family: 'Myriad Pro', 'Gill Sans', 'Gill Sans MT', Calibri, sans-serif; color: #F5911E; text-align:center;\"></br><p style = 'font-size: 35px;'>Muito obrigado!</p> <p style = 'font-size: 30px;'>Sua avaliação está sujeita a aprovação de um moderador.</p></div>");
						});
				});
			}
		else{
			//Caso não esteja logado faz login
			FB.login(function(response) {
				if (response.status === 'connected'){
					FB.api(response.authResponse.userID, {fields: 'link,email,name,picture'}, function(response) {
						var id_usuario = response.id;
						var nome_usuario = response.name;
						var link_perfil = response.link;
						var email_usuario = response.email;
						var link_foto = response.picture.data.url;
						
						$.post("includes-php/detalhes/avalia_empresa.php",{id_usuario: id_usuario, nome_usuario: nome_usuario, link_foto: link_foto, link_perfil: link_perfil, email_usuario: email_usuario, complemento_link: complemento_link, mensagem: mensagem, nota: nota},
							function (data){
								$("#avaliacao-facebook-d-menor").html("<div style = \"font-family: 'Myriad Pro', 'Gill Sans', 'Gill Sans MT', Calibri, sans-serif; color: #F5911E; text-align:center;\"></br><p style = 'font-size: 35px;'>Muito obrigado!</p> <p style = 'font-size: 30px;'>Sua avaliação está sujeita a aprovação de um moderador.</p></div>");
							});
						});
				}
			}, {scope: 'email,user_photos,public_profile'});
		}
	});
};

function avaliar_detalhes_tablet(complemento_link){
	var nota = 0;
	//Pega nota (estrelas)
	if ($('#eA').attr('class') != "icone-avaliacao-detalhes")
		nota++;
	if ($('#eB').attr('class') != "icone-avaliacao-detalhes")
		nota++;
	if ($('#eC').attr('class') != "icone-avaliacao-detalhes")
		nota++;
	if ($('#eD').attr('class') != "icone-avaliacao-detalhes")
		nota++;
	if ($('#eE').attr('class') != "icone-avaliacao-detalhes")
		nota++;
			
	//Pega Mensagem
	var mensagem = $('textarea#fundo-mensagem-detalhes-tablet').val();
			
	//Verifica se o usuário está logado no facebook
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			FB.api(response.authResponse.userID, {fields: 'link,email,name,picture'}, function(response) {
				var id_usuario = response.id;
				var nome_usuario = response.name;
				var link_perfil = response.link;
				var email_usuario = response.email;
				var link_foto = response.picture.data.url;
						
				$.post("includes-php/detalhes/avalia_empresa.php",{id_usuario: id_usuario, nome_usuario: nome_usuario, link_foto: link_foto, link_perfil: link_perfil, email_usuario: email_usuario, complemento_link: complemento_link, mensagem: mensagem, nota: nota},
						function (data){
							$("#avaliacao-facebook-tablet").html("<div style = \"font-family: 'Myriad Pro', 'Gill Sans', 'Gill Sans MT', Calibri, sans-serif; color: #F5911E; text-align:center;\"></br><p style = 'font-size: 35px;'>Muito obrigado!</p> <p style = 'font-size: 30px;'>Sua avaliação está sujeita a aprovação de um moderador.</p></div>");
						});
				});
			}
		else{
			//Caso não esteja logado faz login
			FB.login(function(response) {
				if (response.status === 'connected'){
					FB.api(response.authResponse.userID, {fields: 'link,email,name,picture'}, function(response) {
						var id_usuario = response.id;
						var nome_usuario = response.name;
						var link_perfil = response.link;
						var email_usuario = response.email;
						var link_foto = response.picture.data.url;
						
						$.post("includes-php/detalhes/avalia_empresa.php",{id_usuario: id_usuario, nome_usuario: nome_usuario, link_foto: link_foto, link_perfil: link_perfil, email_usuario: email_usuario, complemento_link: complemento_link, mensagem: mensagem, nota: nota},
							function (data){
								$("#avaliacao-facebook-tablet").html("<div style = \"font-family: 'Myriad Pro', 'Gill Sans', 'Gill Sans MT', Calibri, sans-serif; color: #F5911E; text-align:center;\"></br><p style = 'font-size: 35px;'>Muito obrigado!</p> <p style = 'font-size: 30px;'>Sua avaliação está sujeita a aprovação de um moderador.</p></div>");
							});
						});
				}
			}, {scope: 'email,user_photos,public_profile'});
		}
	});
};

function ver_mais_comentarios(pagina,complemento_link){
	$.post("includes-php/detalhes/ver_mais_comentarios.php",{complemento_link: complemento_link, pagina: pagina},
	function (data){
		dados = eval('(' + data + ')');
		var aux = "<div class='item-comentario-avaliacao'>"; 
		
		var num_avaliacoes = (dados.length-1)/5;
			
		aux += "<div class='item-comentario-avalicoes'>";
		
		var j = 1;
		var cont = 1;
		var indice;
		for (var i = 0; i < num_avaliacoes; i++){
		if (cont == 3){
			indice = 2;
			cont = 0;
		}
		else	
			indice = 1;
			
		cont++;
		aux += "<div class='item-comentario"+indice+"'>";
		aux += "<div id = 'comentario' class='item-modal-num'>";
		aux += "<span class='child item-imagem-avaliacao-usuario'>";
		aux += "<div class='imagem-usuario'><img src = \""+dados[j+1]+"\"/></div>";
		aux += "<div class='font-modal-avaliacao-nome'>"+dados[j]+"</div>";
		aux += "</span>";
		aux += "<span class='item-avaliacao-comentario-detalhes child'>";
		aux += "<div class='font-modal-avaliacao-comentario'>"+dados[j+3]+"</div>";

		aux += "<div class='item-avaliacao-estrelas-detalhes'>";
		aux += "<div class='balao-rodape-avaliacao'>";

		aux += "<div class='numero-avaliacao'>";
		aux += "<div class='text-numero-avaliacao'>"+dados[j+2]+"</div>";
		aux += "</div>";

		aux += "<div class='rodape-estrela'>";
									
		var media = dados[j+2];
		contador_de_estrelas = 0;
		while (media > 0){
			media--;
			if (media >= 0){
				aux += "<div class=\"item-estrela estrela-cheia\"></div>";		
				contador_de_estrelas++;
			}
			else
				if ((media+0.5) == 0){
					aux += "<div class=\"item-estrela estrela-metade\"></div>";	
					contador_de_estrelas++;
					break;
				}
				else
					if ((media+0.5) > 0){
						aux += "<div class=\"item-estrela estrela-cheia\"></div>";	
						contador_de_estrelas++;
						break;
					}
				}
				for (var k = 0; k < (5-contador_de_estrelas); k++)
					aux += "<div class=\"item-estrela estrela-vazia\"></div>";	

					
		data_avaliacao = dados[j+4];
				
		aux += "</div></div></div>";
		aux += "<div class='item-avaliacao-detalhes-estrela-data'>";
		aux += "<div class='font-data-comentario-detalhes child'>"+data_avaliacao+"</div>";
		aux += "</div>";
		aux += "</span>";
		aux += "</div>";
		aux += "<div class='borda-separa-comentario'></div>";
		aux += "</div>";
		j+=5;
	}
				
		aux += "</div></div></div>";	
		
		$("#mais-avaliacoes-d-maior").append(aux);
		
		if (dados[0] > 0)
			$("#item-botao-mais-avaliados").html("<button onclick = \"ver_mais_comentarios("+(pagina+1)+",'"+complemento_link+"');\" id = 'botao-ver-mais-comentarios\' type='button' class='fonte-botao-mais-avaliados botao-mais-avaliados font-botao-detalhes' title='Veja mais comentários!'>Ver Mais</button>");
		else
			$("#item-botao-mais-avaliados").html("");
	});
};

function area_avaliar_indicar(complemento_link){
	//Verifica se o usuário está logado no facebook
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			FB.api(response.authResponse.userID, {fields: 'link,email,name,picture'}, function(response) {
				var id_usuario = response.id;
				$.post("includes-php/detalhes/area_avaliar_indicar.php",{id_usuario: id_usuario, complemento_link: complemento_link},
						function (data){
							dados = eval('(' + data + ')');
							if (dados[0] == true){  // Já indicou
								$("#indicacao-icone-d-maior").hide()
								$("#botao-indicacao-d-maior").hide()
								$("#indicacao-d-maior").show();
							}
							if (dados[1] == true){  // Já avaliou
								//$("#fundo-avaliacao-facebook-d-maior").hide();
								$("#a").hide();
							}
				});
			});
		}
	});
};
function verifica_validade_pagina_facebook(){
	//alert("ERRO >> "+$("#u_0_0").html());
};