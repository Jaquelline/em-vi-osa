<div class='fundo-rodape'>

    <div class="item-rodape">
        <div class="item-logo-rodape">
            <div class="rodape-logo-mg-vicosa" title="Encontre os melhores serviços em Viçosa!"></div>
        </div>
        <div class="item-icones-sociais">
            <a href="https://www.facebook.com/emvicosa" target="_blank">
                <div title='Encontre-nos no Facebook!' class="icone-facebook"></div>
            </a>

            <a href="https://twitter.com/emvicosaoficial" target="_blank">
                <div title='Econtre-nos no twitter!' class="icone-twitter"></div>
            </a>
        </div>
        <a class="item-email-rodape" title="Envie-nos uma mensagem" href="mailto:contato@emvicosa.com.br">contato@emvicosa.com.br</a>
    </div>
</div>