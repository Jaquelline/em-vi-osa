jQuery(document).ready(function ($) {

    var count1 = $("#itemContainer").children().length;

    var count2 = $("#resultado-celular").children().length;

    $("button").click(function () {
        /* get given page */
        var page = parseInt($("input").val());
        /* jump to that page */
        $("div.holder").jPages(page);
    });

    if ($( window ).width() <= 600 && count2 > 0 ){
        $("div.holder").jPages({
            containerID: 'resultado-celular',
            previous: 'anterior',
            next: 'proximo',
            perPage: 1,
            keyBrowse: true,
            scrollBrowse: false,
            animation: "bounceInUp"
        });
    }else{
        if ($( window ).width() <= 600 && count2 == 0 ){

            $("div.holder").jPages({
                containerID: 'resultado-celular',
                previous: '',
                next: '',
                perPage: 1,
                keyBrowse: true,
                scrollBrowse: false,
                animation: "bounceInUp"
            });

        }else{

            if ($( window ).width() > 600 && count1 > 0 ){

                $("div.holder").jPages({
                    containerID: 'itemContainer',
                    previous: 'anterior',
                    next: 'proximo',
                    perPage: 1,
                    keyBrowse: true,
                    scrollBrowse: false,
                    animation: "bounceInUp"
                });
            }else{
                if (!($(window).width() > 600 && count1 == 0)) {
                    $("div.holder").jPages({
                        containerID: 'itemContainer',
                        previous: '',
                        next: '',
                        perPage: 1,
                        keyBrowse: true,
                        scrollBrowse: false,
                        animation: "bounceInUp"
                    });
                }

            }

        }

    }

});
