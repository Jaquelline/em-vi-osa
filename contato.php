<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>emVicosa - Encontre os melhores serviços!</title>
    <link rel="stylesheet" href="lib/jquery-ui/jquery-ui.min.css"/>
    <link rel="stylesheet" href="css/estilo.css"/>
    <link rel="stylesheet" href="css/estilo-desktop-maior.css"/>
    <link rel="stylesheet" href="css/estilo-desktop-menor.css"/>
    <link rel="stylesheet" href="css/estilo-tablet.css"/>
    <link rel="stylesheet" href="css/estilo-celular.css"/>
    <?php include("includes-php/compartilhado/include-favicon.php"); ?>
</head>
<body>
<!-- Menu para desktop-maior, desktop-menor e tablet -->
<nav id='mobile'>
    <div class='fundo-topo'>
        <div class='conteudo-topo'>
            <a href="index.php">
                <div class='logo-mg-vicosa logo-cidade' title="Encontre os melhores serviços em Viçosa!"></div>
            </a>
            <div class='conteudo-item'>
                <a href="index.php">
                    <div class='item-inicio item-inativo'>
                        <div class='icone-inicio-inativo'></div>
                        <div class='font-topo font-inativa'>Início</div>
                    </div>
                </a>
                <a id="item-conquiste-maior">
                    <div class='item-conquiste item-inativo'>
                        <div class='icone-conquiste-inativo'></div>
                        <div class='font-topo font-inativa'>Conquiste + Clientes</div>
                    </div>
                </a>
                <a href="contato.php">
                    <div class='item-contato item-ativo'>
                        <div class='icone-contato-ativo'></div>
                        <div class='font-topo font-ativa'>Contato</div>
                    </div>
                </a>
            </div>
            <!-- Menu para celular -->
            <div class='menu'>
                <div class='icone-menu-mobile mtoggle'></div>
                <div class='texto-menu'>Menu</div>
            </div>
        </div>
    </div>

    <!-- Menu para celular -->
    <ul id='mmenu'>
        <a href="index.php">
            <ul class='item-inicio-menu item-inativo'>
                <li class='icone-inicio-inativo margin-icones-menu'></li>
                <li class='texto-item-menu font-inativa'>Início</li>
            </ul>
        </a>
        <a id="item-conquiste-menor">
            <ul class='item-conquiste-menu item-inativo'>
                <li class='icone-conquiste-inativo margin-icones-menu'></li>
                <li class='texto-item-menu font-inativa'>Conquiste + Clientes</li>
            </ul>
        </a>
        <a href="contato.php">
            <ul class='item-contato-menu item-ativo'>
                <li class='icone-contato-ativo margin-icones-menu'></li>
                <li class='texto-item-menu font-ativa'>Contato</li>
            </ul>
        </a>
    </ul>
</nav>

<div id="modal-conquiste" title="Conquiste Clientes">
    <input type="hidden" autofocus/>
    <div class="item-font-modal-conquiste">
        <span class="font-modalB ">Junte-se a nós! Envie-nos suas informações que entraremos em contato. </span>
    </div>
    <div class="item-modal2">
        <form id="form-cel" name="form-conquiste" action="contato.php" method="post">
            <p class="nome"> <input type="text" id="nomeid" placeholder="Informe seu nome" required="required" name="nome" maxlength="40"/></p>
            <p class="email"> <input type="email" id="emailid" placeholder="Informe seu email" required="required" name="email" /></p>
            <p class="telefone"> <input type="text" id="telefoneid" placeholder="Informe seu telefone" required="required" name="telefone" /></p>
            <p class="empresa"> <input type="text" id="empresaid" placeholder="Informe o nome de sua empresa" required="required" name="empresa" /></p>
            <div id='msgErro' class="font-erro-dialog"></div>
        </form>
    </div>
</div>

<div class='fundo-conteudo'>
    <div class='font-msg-inicial-contato'> Tem alguma dúvida ou sugestão? Estamos aguardando o seu contato!</div>
    <form method = "post" href = "includes-php/envia_email.php" id="form-contato" name="form-contato-tContato">
        <div class="item-info-contato">
            <p class="nome"> <input type="text" id="nomeid-contato" placeholder="Informe seu nome" required="required" name="nome" /></p>
            <p class="email"> <input type="email" id="emailid-contato" placeholder="Informe seu email" required="required" name="email" /></p>
            <p class="assunto"> <input type="text" id="assuntoid-contato" placeholder="Informe o assunto" required="required" name="assunto" maxlength="40" /></p>
        </div>
        <p class="mensagem"><textarea id="mensagem-contato" placeholder="Digite sua mensagem" required="required" name="mensagem" ></textarea></p>
        <div class="item-botao-contato">
            <button class="botao-contato font-botao-detalhes" style = "cursor:pointer" type="submit">Enviar</button>
        </div>

    </form>
</div>

<?php include("includes-php/compartilhado/rodape.php"); ?>

<!-- JavaScript Includes -->
<script src="js/jquery-1.11.2.min.js"></script>
<script src="lib/jquery-ui/jquery-ui.min.js"></script>
<script src="lib/jquery-mask/jquery.maskedinput.js"></script>
<script src="js/validacao-dados.js"></script>
<script src="js/inicial.js"></script>
</body>
</html>