<?php if (isset($_GET['busca'])){ include("includes-php/compartilhado/insere_busca_log_de_buscas.php"); } ?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>emVicosa - Encontre os melhores serviços!</title>
    <link rel="stylesheet" href="lib/jquery-ui/jquery-ui.min.css"/>
    <link rel="stylesheet" href="css/estilo.css"/>
    <link rel="stylesheet" href="css/estilo-desktop-maior.css"/>
    <link rel="stylesheet" href="css/estilo-desktop-menor.css"/>
    <link rel="stylesheet" href="css/estilo-tablet.css"/>
    <link rel="stylesheet" href="css/estilo-celular.css"/>
    <link rel="stylesheet" href="lib/jquery.pages/style.css"/>
    <?php include("includes-php/compartilhado/include-favicon.php"); ?>
</head>
<body>
<!-- Menu para desktop-maior, desktop-menor e tablet -->
<nav id='mobile'>
    <div class='fundo-topo'>
        <div class='conteudo-topo'>
            <a href="index.php">
                <div class='logo-mg-vicosa logo-cidade' title="Encontre os melhores serviços em Viçosa!"></div>
            </a>
            <div class='conteudo-item'>
                <a href="index.php">
                    <div class='item-inicio item-inativo'>
                        <div class='icone-inicio-inativo'></div>
                        <div class='font-topo font-inativa'>Início</div>
                    </div>
                </a>
                <a id="item-conquiste-maior">
                    <div class='item-conquiste item-inativo'>
                        <div class='icone-conquiste-inativo'></div>
                        <div class='font-topo font-inativa'>Conquiste Clientes</div>
                    </div>
                </a>
                <a href="contato.php">
                    <div class='item-contato item-inativo'>
                        <div class='icone-contato-inativo'></div>
                        <div class='font-topo font-inativa'>Contato</div>
                    </div>
                </a>
            </div>
            <!-- Menu para celular -->
            <div class='menu'>
                <div class='icone-menu-mobile mtoggle'></div>
                <div class='texto-menu'>Menu</div>
            </div>
        </div>
    </div>

    <!-- Menu para celular -->
    <ul id='mmenu'>
        <a href="index.php">
            <ul class='item-inicio-menu item-ativo'>
                <li class='icone-inicio-ativo margin-icones-menu'></li>
                <li class='texto-item-menu font-ativa'>Início</li>
            </ul>
        </a>
        <a id="item-conquiste-menor">
            <ul class='item-conquiste-menu item-inativo'>
                <li class='icone-conquiste-inativo margin-icones-menu'></li>
                <li class='texto-item-menu font-inativa'>Conquiste Clientes</li>
            </ul>
        </a>
        <a href="contato.php">
            <ul class='item-contato-menu item-inativo'>
                <li class='icone-contato-inativo margin-icones-menu'></li>
                <li class='texto-item-menu font-inativa'>Contato</li>
            </ul>
        </a>
    </ul>
</nav>

<div id="modal-conquiste" title="Conquiste Clientes">
    <input type="hidden" autofocus/>
    <div class="item-font-modal-conquiste">
        <span class="font-modalB ">Junte-se a nós! Envie-nos suas informações que entraremos em contato. </span>
    </div>
    <div class="item-modal2">
        <form id="form-cel" name="form-conquiste">
            <p class="nome"> <input type="text" id="nomeid" placeholder="Informe seu nome" required="required" name="nome" /></p>
            <p class="email"> <input type="text" id="emailid" placeholder="Informe seu email" required="required" name="email" /></p>
            <p class="telefone"> <input type="text" id="telefoneid" placeholder="Informe seu telefone" required="required" name="telefone" /></p>
            <p class="empresa"> <input type="text" id="empresaid" placeholder="Informe o nome de sua empresa" required="required" name="empresa" /></p>
            <button type="submit">Enviar</button>
        </form>
    </div>
</div>

<div class='fundo-conteudo'>
    <div class='area-total'>
        <div class="area-pesquisa">
            <input id = "input-area-pesquisa" type="text" class='font-area' placeholder="O que deseja encontrar em Viçosa?"/>
			<div id = "sugestoes-area-pesquisa" class = "sugestoes-area-pesquisa">
			</div>
        </div>
		
        <div class="area-botao">
            <button type="button" id = "botao-pesquisa" class="botao-pesquisa font-botao-pesquisa">Pesquisar</button>
            <!--Aqui fica o botao categoria-->
        </div>
    </div>
	
    <div class="item-topo-resultados">
        <div class="font-topo-resultados">Resultados para: <?php if (isset($_GET['busca'])){ echo $_GET['busca'];} ?> </div>
    </div>

    <!-- item container -->
    <div id="itemContainer">
	<?php include("includes-php/resultadoBusca/carrega_resultado_busca_desktop.php"); ?>
    </div>

    <div id="resultado-celular">
	<?php include("includes-php/resultadoBusca/carrega_resultado_busca_celular.php"); ?>
    </div>

    <div align="center">
        <div class="holder"></div>
    </div>

</div>

<?php include("includes-php/compartilhado/rodape.php"); ?>

<!-- JavaScript Includes -->
<script src="js/jquery-1.11.2.min.js"></script>
<script src="lib/jquery-ui/jquery-ui.min.js"></script>
<!-- jPages -->
<script defer src="lib/jquery.pages/jPages.min.js"></script>
<script src="js/inicial.js"></script>
<script src="js/resultadoBusca.js"></script>


</body>
</html>