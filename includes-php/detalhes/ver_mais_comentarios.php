<?php
	//Caso alguém que não seja o sistema tente acessar o script redireciona para a página inicial
	if (isset($_POST['complemento_link']) && isset($_POST['pagina'])){	
		$complemento_link = $_POST['complemento_link'];
		$pagina = $_POST['pagina'];
	}
	else
		header("location:index.php");
		
	$num_de_empresas = 9;
		
	//Abre banco de dados
	require_once("../compartilhado/conexao.php");
	
	$offset = ($num_de_empresas*($pagina-1));
	
	//Busca avaliações
	$query = "SELECT usuarios.nomeUsuario,usuarios.linkFotoFacebook,avaliacoes.notaAvaliacao,avaliacoes.justificativaAvaliacao,avaliacoes.dataAvaliacao 
	FROM avaliacoes,empresas,usuarios 
	WHERE avaliacoes.idEmpresa = empresas.idEmpresa and avaliacoes.idUsuario = usuarios.idUsuario and empresas.complementoLinkEmVicosa = '$complemento_link' 
	ORDER BY avaliacoes.dataAvaliacao LIMIT $num_de_empresas OFFSET $offset";
	$result1 = mysql_query($query,$connection) or die(mysql_error());

	//Busca avaliações da próxima página
	$aux = ($num_de_empresas*$pagina);
	$query = "SELECT usuarios.nomeUsuario,usuarios.linkFotoFacebook,avaliacoes.notaAvaliacao,avaliacoes.justificativaAvaliacao,avaliacoes.dataAvaliacao 
	FROM avaliacoes,empresas,usuarios 
	WHERE avaliacoes.idEmpresa = empresas.idEmpresa and avaliacoes.idUsuario = usuarios.idUsuario and empresas.complementoLinkEmVicosa = '$complemento_link' 
	ORDER BY avaliacoes.dataAvaliacao LIMIT $num_de_empresas OFFSET $aux";
	$result2 = mysql_query($query,$connection) or die(mysql_error());
	
	$resposta = array();
	//Guarda número de avaliações da próxima página
	$resposta[0] = mysql_num_rows($result2);
	
	$row = array();
	$i = 0;
	while ($row = mysql_fetch_assoc($result1)) {
		$resposta[$i+1] = $row["nomeUsuario"];
		$resposta[$i+2] = $row["linkFotoFacebook"];
		$resposta[$i+3] = $row["notaAvaliacao"];
		$resposta[$i+4] = $row["justificativaAvaliacao"];
		list($data,$hora) = explode(" ",$row["dataAvaliacao"]);
		list($ano,$mes,$dia) = explode("-",$data);
		$resposta[$i+5] = $dia."/".$mes."/".$ano;
		$i+=5;
	}
	
	echo json_encode($resposta);
?>