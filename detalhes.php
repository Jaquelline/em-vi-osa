<!DOCTYPE html>
<html xmlns="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<!--Nome da empresa-->
    <title><?php include("includes-php/detalhes/carrega_nome_empresa.php");?> - emViçosa</title>
	<!--/Nome da empresa-->
    <link rel="stylesheet" href="lib/jquery.bxslider/jquery.bxslider.css"/>
    <link rel="stylesheet" href="lib/jquery-ui/jquery-ui.min.css"/>
    <link rel="stylesheet" href="css/estilo.css"/>
    <link rel="stylesheet" href="css/estilo-desktop-maior.css"/>
    <link rel="stylesheet" href="css/estilo-desktop-menor.css"/>
    <link rel="stylesheet" href="css/estilo-tablet.css"/>
    <link rel="stylesheet" href="css/estilo-celular.css"/>
	<script>
		//CHAMA API DO FACEBOOK
		window.fbAsyncInit = function() {
		FB.init({
			appId      : '1715998728621862',
			xfbml      : true,
			version    : 'v2.4'
			});
		};

		(function(d, s, id){
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	
    <?php include("includes-php/compartilhado/include-favicon.php"); ?>
</head>
<body>

<div id="fb-root"></div>

<nav id='mobile'>
    <div class='fundo-topo'>
        <div class='conteudo-topo'>
            <a href="index.php">
                <div class='logo-mg-vicosa logo-cidade' title="Encontre os melhores serviços em Viçosa!"></div>
            </a>
            <div class='conteudo-item'>
                <a href="index.php">
                    <div class='item-inicio item-inativo'>
                        <div class='icone-inicio-inativo'></div>
                        <div class='font-topo font-inativa'>Início</div>
                    </div>
                </a>
                <a id="item-conquiste-maior">
                    <div class='item-conquiste item-inativo'>
                        <div class='icone-conquiste-inativo'></div>
                        <div class='font-topo font-inativa'>Conquiste + Clientes</div>
                    </div>
                </a>
                <a href="contato.php">
                    <div class='item-contato item-inativo'>
                        <div class='icone-contato-inativo'></div>
                        <div class='font-topo font-inativa'>Contato</div>
                    </div>
                </a>
            </div>
            <!-- Menu para celular -->
            <div class='menu'>
                <div class='icone-menu-mobile mtoggle'></div>
                <div class='texto-menu'>Menu</div>
            </div>
        </div>
    </div>

    <!-- Menu para celular 2-->
    <ul id='mmenu'>
        <a href="index.php">
            <ul class='item-inicio-menu item-inativo'>
                <li class='icone-inicio-inativo margin-icones-menu'></li>
                <li class='texto-item-menu font-inativa'>Início</li>
            </ul>
        </a>
        <a id="item-conquiste-menor">
            <ul class='item-conquiste-menu item-inativo'>
                <li class='icone-conquiste-inativo margin-icones-menu'></li>
                <li class='texto-item-menu font-inativa'>Conquiste + Clientes</li>
            </ul>
        </a>
        <a href="contato.php">
            <ul class='item-contato-menu item-inativo'>
                <li class='icone-contato-inativo margin-icones-menu'></li>
                <li class='texto-item-menu font-inativa'>Contato</li>
            </ul>
        </a>
    </ul>
</nav>

<div id="modal-conquiste" title="Conquiste Clientes">
    <input type="hidden" autofocus/>
    <div class="item-font-modal-conquiste">
        <span class="font-modalB ">Junte-se a nós! Envie-nos suas informações que entraremos em contato. </span>
    </div>
    <div class="item-modal2">
        <form id="form-cel" name="form-conquiste">
            <p class="nome"> <input type="text" id="nomeid" placeholder="Informe seu nome" required="required" name="nome" maxlength="40"/></p>
            <p class="email"> <input type="text" id="emailid" placeholder="Informe seu email" required="required" name="email" /></p>
            <p class="telefone"> <input type="text" id="telefoneid" placeholder="Informe seu telefone" required="required" name="telefone" /></p>
            <p class="empresa"> <input type="text" id="empresaid" placeholder="Informe o nome de sua empresa" required="required" name="empresa" /></p>
            <div id='msgErro' class="font-erro-dialog"></div>
        </form>
    </div>
</div>

<div class='fundo-conteudo'>
	
    <div id="tela-celular-tablet">
		<?php include("includes-php/detalhes/carrega_detalhes_tablet.php"); ?>
    </div>
	
    <div id="tela-desktop-menor">
		<?php include("includes-php/detalhes/carrega_detalhes_desktop_menor.php"); ?>
    </div>
	
    <div id="tela-desktop-maior">
		<?php include("includes-php/detalhes/carrega_detalhes_desktop_maior.php"); ?>
	</div>

<?php include("includes-php/compartilhado/rodape.php"); ?>

<!--JavaScript Includes -->
<script src="js/jquery-1.11.2.min.js"></script>
<script src="lib/jquery-ui/jquery-ui.min.js"></script>
<script src="lib/jquery-mask/jquery.maskedinput.js"></script>
<script src="js/validacao-dados.js"></script>
<script src="js/inicial.js"></script>
<script src="js/eventPause.js"></script>
<!-- bxSlider -->
<script defer src="lib/jquery.bxslider/jquery.bxslider.min.js"></script>
<script src="js/detalhes.js"></script>
<?php  echo "<script> area_avaliar_indicar('".$_GET['busca']."'); </script>"; ?>
</body>
</html>