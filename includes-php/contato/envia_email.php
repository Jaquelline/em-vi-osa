<?php
	/* Verifica qual é o sistema operacional do servidor para ajustar o cabeçalho de forma correta.  */
	if (PATH_SEPARATOR == ";") 
		$quebra_linha = "\r\n"; //Se for Windows
	else 
		$quebra_linha = "\n"; //Se "nÃ£o for Windows"
 
	// Passando os dados obtidos pelo formulário para as variáveis abaixo
	$nomeremetente     = $_POST['nome'];
	$emailremetente    = $_POST['email'];
	$emaildestinatario = "contato@emvicosa.com.br";
	$assunto = $_POST['assunto'];
	
	//Verifica se não foi passado um assunto
	if (empty($assunto))
		$assunto = "<Sem assunto>";
		
	$mensagem = $_POST['mensagem'];
 
 
	/* Montando a mensagem a ser enviada no corpo do e-mail. */
	$mensagemHTML = "<P>Aqui está a mensagem postada por você; formatada em HTML:</P>
	<p><b><i>".$mensagem."</i></b></p>
	<hr>";
 
 
	/* Montando o cabeçalho da mensagem */
	$headers = "MIME-Version: 1.1" .$quebra_linha;
	$headers .= "Content-type: text/html; charset=iso-8859-1" .$quebra_linha;
	// Perceba que a linha acima contém "text/html", sem essa linha, a mensagem não chegará formatada.
	$headers .= "From: " . $emailremetente.$quebra_linha;
	$headers .= "Reply-To: " . $emailremetente . $quebra_linha;
	// Note que o e-mail do remetente será usado no campo Reply-To (Responder Para)
 
	/* Enviando a mensagem */
	//É obrigatório o uso do parâmetro -r (concatenação do "From na linha de envio"), aqui na Locaweb:
	if(!mail($emaildestinatario, $assunto, $mensagemHTML, $headers ,"-r".$emailremetente)){ // Se for Postfix
		$headers .= "Return-Path: " . $emailremetente . $quebra_linha; // Se "não for Postfix"
		mail($emaildestinatario, $assunto, $mensagemHTML, $headers );
	}
?>