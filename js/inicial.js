jQuery(document).ready(function($) {

    /*Controla o menu do celular*/
    $("#mmenu").hide();

    $(".menu").click(function() {
        $("#mmenu").slideToggle(500);
    });

    /*Variaveis que guardam o tamanho das modais*/
    var width_modal_1 = 0;

    var height_modal_1 = 'auto';


    /*Esta funcao define o tamanho das modais em cada tela*/
    $(function(){

        if ($( window ).width() <= 600){
            width_modal_1 = 300;
        }

        if ($( window ).width() >= 601 && $( window ).width() <= 800){
            width_modal_1 = 320;
        }

        if ($( window ).width() >= 801 && $( window ).width() <= 1200){
            width_modal_1 = 400;
        }

        if ($( window ).width() >= 1201){
            width_modal_1 = 500;
        }

    });

    /*Altera o tamanho da modal de acordo com o tamanho da tela*/
    $( window ).resize(function() {

        if ($( window ).width() <= 600){
            width_modal_1 = 300;
        }

        if ($( window ).width() >= 601 && $( window ).width() <= 800){
            width_modal_1 = 320;
        }

        if ($( window ).width() >= 801 && $( window ).width() <= 1200){
            width_modal_1 = 400;
        }

        if ($( window ).width() >= 1201){
            width_modal_1 = 500;
        }

        $("#modal-conquiste").dialog( "option", "width", width_modal_1 );

    });

    /*Configuracoes modal conquiste mais clientes*/
    $(function() {
        $("#modal-conquiste").dialog({
            autoOpen: false,
            modal: true,
            width: width_modal_1,
            height: height_modal_1,
            open: function () {
                $(this).find("[type=submit]").hide();

                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery('#modal-conquiste').dialog('close');
                    $("#msgErro").empty();
                    $("#empresaid, #telefoneid, #nomeid, #emailid").val("");
                })

            },
            buttons: [
                {
                    id: "ev",
                    text: "Enviar",
                    click: function () {
                        if ($("#nomeid").val().length === 0){
                            $("#msgErro").empty();
                            $("#msgErro").text("Preencha o campo nome!");
                        }else{
                            if ($("#emailid").val().length === 0){
                                $("#msgErro").empty();
                                $("#msgErro").text("Preencha o campo email!");
                            }else{
                                if(!isValidEmailAddress($("#emailid").val())){

                                    $("#msgErro").empty();
                                    $("#msgErro").text("Email inválido!");
                                }else{
                                    if ($("#telefoneid").val().length === 0){
                                        $("#msgErro").empty();
                                        $("#msgErro").text("Preencha o campo telefone!");
                                    }else{
                                        if ($("#empresaid").val().length === 0){
                                            $("#msgErro").empty();
                                            $("#msgErro").text("Preencha o campo empresa!");
                                        }
										else{
											var nome = $("#nomeid").val();
											var email = $("#emailid").val();
											var telefone = $("#telefoneid").val();
											var empresa = $("#empresaid").val();
											
											$.post("includes-php/conquiste_clientes.php",
												{
													nome:nome,
													email:email,
													telefone:telefone,
													empresa:empresa
												},
												function (data){
													$("#msgErro").empty();
													$("#empresaid, #telefoneid, #nomeid, #emailid").val("");
													$("#modal-conquiste").dialog("close");
												}
											);
										}
                                    }

                                }

                            }
                        }

                    },
                    type: "submit",
                    form: "form-conquiste" // <-- Make the association
                },
                {
                    text: "Voltar",
                    click: function () {
                        $("#msgErro").empty();
                        $("#empresaid, #telefoneid, #nomeid, #emailid").val("");
                        $(this).dialog("close");

                    }
                }
            ]
        });


    });


    /*Quando clica do celular*/
    $("#item-conquiste-menor" ).click(function() {
        $("#modal-conquiste" ).dialog( "open" );
    });

    /*Quando clica do tablet, desktop menor e maior*/
    $("#item-conquiste-maior" ).click(function() {
        $("#modal-conquiste" ).dialog( "open" );
    });

});


//Controla input de pesquisa
$(document).ready(function(){
	$("#botao-pesquisa").click(
		function(){
			var busca = $("#input-area-pesquisa").val();
			if (busca != "")
				location.href = "resultadoBusca.php?busca=" + busca;
		}
	);
	
	$("#input-area-pesquisa").keyup(
		function(e){
		var busca = $("#input-area-pesquisa").val();

		if (e.keyCode == 13 && busca != "")
				location.href = "resultadoBusca.php?busca=" + busca;
		
		if ((e.keyCode == 8 || e.keyCode == 13) && busca == ""){
			$("#sugestoes-area-pesquisa").hide();
		}
		else{
			$.post("includes-php/compartilhado/busca_empresas.php",{busca: busca},
			function(empresas){
				empresas = eval('(' + empresas + ')');
				var aux = "<table cellspacing='5'>";
				var num_empresas = (empresas.length/2);
				if (num_empresas > 8)
					num_empresas = 8;
				for (var i = 0; i < (num_empresas*2); i+=2)
					aux += "<tr><td><a href = 'detalhes.php?busca=" + empresas[i+1] + "'><span class = 'fonte-sugestoes-area-pesquisa'>" + empresas[i] + "</span></a></td></tr>";
				aux += "</table>";
			
				//Limpa área de sugestões de empresas
				$("#sugestoes-area-pesquisa").html("");
			
				if (num_empresas > 0){
					//Mostra a div
					$("#sugestoes-area-pesquisa").show();
					//Preenche com as sugestões de empresas da área de pesquisa
					$("#sugestoes-area-pesquisa").html(aux);
				}
				else
					$("#sugestoes-area-pesquisa").hide();
			});
		}
	}
	);
});

function ver_mais(pagina){
	//Verifica qual o número de empresas deve ser exibido
	var num_de_empresas;
	if ($('#desktop-maior').is(':visible'))
		num_de_empresas = 8;
	else
	if ($('#desktop-menor').is(':visible'))
		num_de_empresas = 6;
	else
		num_de_empresas = 4;

	$.post("includes-php/inicial/ver_mais.php",{num_de_empresas:num_de_empresas, pagina:pagina},
		function(empresas){
			empresas = eval('(' + empresas + ')');
			
			//Seta o número da página 
			if (empresas[0] == 0)
				$("#item-botao-mais-avaliados").html("");
			else{
				$("#item-botao-mais-avaliados").html("<button onclick = \"ver_mais(" + (pagina+1) + ")\" id = \"botao-mais-avaliados\"" + 
				"type=\"button\" class=\"fonte-botao-mais-avaliados botao-mais-avaliados\" title=\"Veja mais serviços\">Ver Mais</button>");
			}
		
			var aux = "";
			for (var i = 1; i < empresas.length; i+=5){
				aux += "<a href = detalhes.php?busca="+empresas[i+2]+"><div class=\"balao-mais-avaliados\" title=\"" + empresas[i] + "\">";
				aux += "<div  class=\"conteudo-balao\">";
				aux += "<div class=\"item-conteudo\"><img src=\"img/mg/vicosa/estabelecimentos/" + empresas[i+1] + "\"></div>";
				aux += "</div>";
				aux += "<div class=\"rodape-balao\">";
				aux += "<div class=\"rodape-avaliacao\">";
				aux += "<div class=\"balao-rodape-avaliacao\">";

				aux += "<div class=\"numero-avaliacao\">";
				aux += "<div class=\"text-numero-avaliacao\">" + empresas[i+4] + "</div>";
				aux += "</div>";

				aux += "<div class=\"rodape-estrela\">";
				var media = empresas[i+4];
				var contador_de_estrelas = 0;
				while (media > 0){
					media--;
					if (media >= 0){
						aux += "<div class=\"item-estrela estrela-cheia\"></div>";		
						contador_de_estrelas++;
					}
					else
						if ((media+0.5) == 0){
							aux += "<div class=\"item-estrela estrela-metade\"></div>";	
							contador_de_estrelas++;
							break;
						}
						else
							if ((media+0.5) > 0){
								aux += "<div class=\"item-estrela estrela-cheia\"></div>";	
								contador_de_estrelas++;
								break;
							}
				}
				for (var j = 0; j < (5-contador_de_estrelas); j++)
					aux += "<div class=\"item-estrela estrela-vazia\"></div>";
				aux += "</div>";

				aux += "</div>";
				aux += "</div>";
				aux += "<div class=\"rodape-indicacao\">";
				aux += "<div class=\"fundo-indicar\">";
				aux += "<div class=\"item-mao\">";
				aux += "<div class=\"mao-indica\"></div>";
				aux += "</div>";

				aux += "<div class=\"item-numero-indicacao\">";
				aux += "<div class=\"text-numero-indicacao\">" + empresas[i+3] + "</div>";
				aux += "</div>";
				aux += "</div>";
				aux += "</div>";
				aux += "</div>";
				aux += "</div></a>";
			}
			if ($('#desktop-maior').is(':visible'))
				$("#empresas1").append(aux);
			else
			if ($('#desktop-menor').is(':visible'))
				$("#empresas2").append(aux);
			else
			if ($('#tablet').is(':visible'))
				$("#empresas3").append(aux);
			else
			if ($('#celular').is(':visible'))
				$("#empresas4").append(aux);
			
			window.scrollTo(0,100000000);
		});
};