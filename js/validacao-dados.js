jQuery(document).ready(function($) {

    $(".nome").keypress(function(e){

        var tecla = new Number();

        if(window.event) {
            tecla = e.keyCode;
        }else if(e.which) {
            tecla = e.which;
        }else {
            return true;
        }
        if((tecla >= "48") && (tecla <= "57")){
            return false;
        }

    });

    $("#telefoneid").mask("(99) 9999-9999?9");

    $("#telefoneid").on("blur", function() {
        var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );

        if( last.length == 5 ) {
            var move = $(this).val().substr( $(this).val().indexOf("-") + 1, 1 );

            var lastfour = last.substr(1,4);

            var first = $(this).val().substr( 0, 9 );

            $(this).val( first + move + '-' + lastfour );
        }
    });


});
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    return pattern.test(emailAddress);
};